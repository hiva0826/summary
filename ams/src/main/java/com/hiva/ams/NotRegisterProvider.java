package com.hiva.ams;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;

import com.hiva.ams.not_register.ActivityNotRegister;
import com.hiva.ams.not_register.ActivityNotRegister2;
import com.hiva.ams.not_register.ActivityNotRegister3;
import com.hiva.ams.not_register.ActivityNotRegister4;
import com.hiva.ams.not_register.ActivityNotRegister5;
import com.hiva.helper.log.LogHelper;

/**
 * Create By HuangXiangXiang 2022/12/12
 */
public class NotRegisterProvider extends ContentProvider {


    @Override
    public boolean onCreate() {

        final ActivityNotRegister register ;
        int sdk = Build.VERSION.SDK_INT;
        if(sdk <= 25){
            register = new ActivityNotRegister();
        }else if(sdk <= 27){
            register = new ActivityNotRegister2();
        }else if(sdk <= 28){
            register = new ActivityNotRegister3();
        }else if(sdk <= 29){
            register = new ActivityNotRegister4();
        }else {
            register = new ActivityNotRegister5();
        }
        boolean result = register.init(getContext());
        LogHelper.i(result);

        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        return null;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }
}
