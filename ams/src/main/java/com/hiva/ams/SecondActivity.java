package com.hiva.ams;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.hiva.helper.log.LogHelper;

/**
 * Create By HuangXiangXiang 2022/11/28
 */
public class SecondActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        LogHelper.i();

        FirstActivity.init(this);
        finish();
    }


    public void first(View view) {

        Intent intent = new Intent();
        intent.setClass(this, FirstActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }

    @Override
    protected void onStart() {
        super.onStart();
        LogHelper.i();
    }

    @Override
    protected void onResume() {
        super.onResume();
        LogHelper.i();
    }

    @Override
    protected void onPause() {
        super.onPause();
        LogHelper.i();
    }

    @Override
    protected void onStop() {
        super.onStop();
        LogHelper.i();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LogHelper.i();
    }
}
