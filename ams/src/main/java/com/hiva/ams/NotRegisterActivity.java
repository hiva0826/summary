package com.hiva.ams;

import android.app.Activity;
import android.os.Bundle;

/**
 * Create By HuangXiangXiang 2022/12/9
 *
 * 不在AndroidManifest注册的Activity
 */
public class NotRegisterActivity extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_not_register);
    }
}
