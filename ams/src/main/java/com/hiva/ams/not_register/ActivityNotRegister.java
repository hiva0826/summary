package com.hiva.ams.not_register;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Message;

import com.hiva.helper.clazz.ClassUtils;
import com.hiva.helper.log.LogHelper;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * Create By HuangXiangXiang 2022/12/9
 *
 * VERSION.SDK_INT [23 24 25]
 *
 *
 */
public class ActivityNotRegister {

    /**真实的activity*/
    private final String KEY_REAL_ACTIVITY = "realActivity" ;

    public ActivityNotRegister(){
        LogHelper.i(this);
    }


    /**
     * 初始化
     * 可以 直接调用不在Manifest中的Activity
     * */
    public final boolean init(Context context) {

        PackageManager packageManager = context.getPackageManager() ;
        String packageName = context.getPackageName() ;
        ComponentName defaultCN = getDefault(packageManager, packageName) ;
        if(defaultCN == null){
            return false;
        }
        boolean result = initToAMS(packageManager, packageName, defaultCN);
        if(!result){
            return false;
        }
        return initFromAMS() ;
    }

    /**
     *
     * 获取在Manifest注册过 Activity
     * */
    private ComponentName getDefault(PackageManager packageManager, String packageName)  {

        Intent intent = packageManager.getLaunchIntentForPackage(packageName) ; // 优先使用launcher
        if(intent != null){
            ComponentName cn = intent.getComponent() ;
            if(cn != null){
                return cn ;
            }
        }
        PackageInfo pi = null;
        try {
            pi = packageManager.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if(pi != null){
            ActivityInfo[] activities = pi.activities;
            if(activities != null && activities.length > 1){
                ActivityInfo activityInfo = activities[0] ;
                return new ComponentName(activityInfo.packageName, activityInfo.name) ;
            }
        }
        return null ;
    }


    /**
     * 代理 对象
     * */
    private Object proxyObject(InvocationHandler handler, Object object) {

        Class<?>[] interfaces = (Class<?>[]) object.getClass().getGenericInterfaces();
        return Proxy.newProxyInstance( ClassLoader.getSystemClassLoader(), interfaces, handler) ;
    }


    /**返回结果*/
    protected static class Result{

        private final Object _object;
        private final Field _field; // 为Object类型

        public Result(Object object, Field field) {

            this._object = object;
            this._field = field;
        }

        private Object getObject(){

            try {
                return _field.get(_object);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null ;
        }

        private boolean setObject(Object object){
            try {
                _field.set(this._object, object);
                return true;
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            return false;
        }


    }

    private boolean initToAMS(final PackageManager packageManager, final String packageName, final ComponentName defaultCN){

        final Result result = onInitToAMS(); // 获取对象代理
        if(result == null){
            return false;
        }
        final Object object = result.getObject();
        if(object == null){
            return false;
        }

        InvocationHandler toAMS = new InvocationHandler() {

            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

                Intent intent = onFindIntentFromToAMS(method, args);
                if(intent != null){
                    storeRealActivity(intent, packageManager, packageName, defaultCN);
                }

                return method.invoke(object, args);
            }
        };

        Object proxyObject = proxyObject(toAMS, object);
        return result.setObject(proxyObject);
    }

    private boolean initFromAMS(){

        final Result result = onInitFromAMS(); // 获取 AMS 返回
        if(result == null){
            return false;
        }

        final Object object = result.getObject();
        final Object proxyObject;
        if(object == null){ //代理对象为空，直接new一个
            proxyObject = new Handler.Callback() {
                @Override
                public boolean handleMessage(Message msg) {
                    resumeRealActivity(msg);
                    return false;
                }
            };
        }else {

            InvocationHandler fromAMS = new InvocationHandler() {
                @Override
                public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

                    if("handleMessage".equals(method.getName())){
                        Message msg = (Message) args[0];
                        resumeRealActivity(msg) ;
                    }
                    return method.invoke(object, args);
                }
            };
            proxyObject = proxyObject(fromAMS, object);
        }
        return result.setObject(proxyObject);
    }

    /**
     * 重新包装 真实的Activity
     * */
    private void storeRealActivity(Intent intent, PackageManager packageManager, String packageName, ComponentName defaultCN) {

        ComponentName cn = intent.getComponent(); // 获取真实意图
        if(cn != null){

            //判断 是否在 Manifest注册
            ActivityInfo activityInfo = null ;
            try{
                activityInfo = packageManager.getActivityInfo(cn,PackageManager.GET_META_DATA) ;
            }catch (Exception ignored){}

            if(activityInfo == null ){ // 没有在 Manifest注册

                if(packageName.equals(cn.getPackageName())){ // 当前应用才能效果
                    String className = cn.getClassName() ;
                    Class<?> cls = null ;
                    try{
                        cls = Class.forName(className) ;
                    }catch (Exception ignored){
                    }
                    if(ClassUtils.isSonClass(Activity.class, cls)){ // 是Activity的子类

                        intent.putExtra(KEY_REAL_ACTIVITY, intent.getComponent()) ; // 真实的备份一下
                        intent.setComponent(defaultCN) ;
                    }
                }
            }
        }



    }

    /**
     * 重新恢复 真实的Activity
     * */
    private void resumeRealActivity(Message msg) {

        Intent intent = onFindIntentFromFromAMS(msg) ;
        if(intent != null){
            ComponentName cn = intent.getParcelableExtra(KEY_REAL_ACTIVITY);
            if(cn != null){
                intent.setComponent(cn) ;
            }
        }
    }

    protected Result onInitToAMS() {

        try {
            Class<?> cls = Class.forName("android.app.ActivityManagerNative");
            Field field = cls.getDeclaredField("gDefault") ;
            field.setAccessible(true);

            Object gDefault = field.get(null) ;  // 获取 Singleton 实例 gDefault

            cls = Class.forName("android.util.Singleton");

            Method getMethod = cls.getMethod("get"); // 防止第一次获取为空
            getMethod.invoke(gDefault);

            field = cls.getDeclaredField("mInstance") ;
            field.setAccessible(true);

            return new Result(gDefault, field);

        } catch (Exception e ) {
            e.printStackTrace();
        }

        return null;
    }

    protected Result onInitFromAMS() {

        try {
            Class<?> cls = Class.forName("android.app.ActivityThread") ;
            Field field = cls.getDeclaredField("sMainThreadHandler") ;
            field.setAccessible(true);
            Handler sMainThreadHandler = (Handler) field.get(null);

            cls = Handler.class ;
            field = cls.getDeclaredField("mCallback") ;
            field.setAccessible(true);

            return new Result(sMainThreadHandler, field);

        }catch (Exception e){
            e.printStackTrace();
        }

        return null;
    }

    /**
     * 从 发送给AMS的信息找到Intent对象
     *
     * */
    protected Intent onFindIntentFromToAMS(Method method, Object[] args) {

        if("startActivity".equals(method.getName())){
            return (Intent) args[2];
        }
        return null;
    }


    /**
     * 从 来自AMS的信息找到Intent对象
     * */
    protected Intent onFindIntentFromFromAMS(Message msg) {

        if(msg.what == 100){

            Object obj = msg.obj;
            Class<?> cls = obj.getClass() ;
            try {
                Field field = cls.getDeclaredField( "intent");
                field.setAccessible(true);
                return  (Intent) field.get(obj);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return null;
    }

}
