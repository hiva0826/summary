package com.hiva.ams.not_register;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Create By HuangXiangXiang 2022/12/9
 *
 * VERSION.SDK_INT  == [29]
 *
 */
public class ActivityNotRegister4 extends ActivityNotRegister3 {

    @Override
    protected Result onInitToAMS() {

        try {
            Class<?> cls = Class.forName("android.app.ActivityTaskManager");
            Field field = cls.getDeclaredField("IActivityTaskManagerSingleton") ;
            field.setAccessible(true);

            Object iActivityTaskManagerSingleton = field.get(null) ;  // 获取 Singleton 实例 IActivityManagerSingleton

            cls = Class.forName("android.util.Singleton");

            Method getMethod = cls.getMethod("get"); // 防止第一次获取为空
            getMethod.invoke(iActivityTaskManagerSingleton);


            field = cls.getDeclaredField("mInstance") ;
            field.setAccessible(true);

            return new Result(iActivityTaskManagerSingleton, field);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null ;
    }

}
