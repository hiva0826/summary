package com.hiva.ams;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;

import com.hiva.ams.not_register.ActivityNotRegister;
import com.hiva.ams.not_register.ActivityNotRegister2;
import com.hiva.ams.not_register.ActivityNotRegister3;
import com.hiva.ams.not_register.ActivityNotRegister4;
import com.hiva.ams.not_register.ActivityNotRegister5;
import com.hiva.helper.log.LogHelper;

import java.util.List;

/**
 * Create By HuangXiangXiang 2022/11/28
 */
public class FirstActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogHelper.i();
        setContentView(R.layout.activity_first);
        Settings.Secure.getFloat()
        init(this);
    }

    public static class Test{

    }

    public void second(View view) {

        Intent intent = new Intent();
        intent.setClass(this, SecondActivity.class);

//        Bundle bundle = new Bundle();
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        try {
//            intent.setClassName("com.hiva", "");
//            intent.setClassName("com.svox.pico", "123");
            startActivity(intent);
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public void startNotRegisterActivity(View view){

        Intent intent = new Intent(this, NotRegisterActivity.class);
        startActivity(intent);
    }

    public static void init(Activity context){

        ActivityManager am = (ActivityManager) context.getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> list = am.getRunningTasks(1 ) ;
        final int size = list.size();
        LogHelper.i(size);
        for (int i = 0; i < size; i++) {

            ActivityManager.RunningTaskInfo recentTaskInfo = list.get(i);
            LogHelper.i(recentTaskInfo.numActivities);

        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        LogHelper.i();
    }

    @Override
    protected void onResume() {
        super.onResume();
        LogHelper.i();
    }

    @Override
    protected void onPause() {
        super.onPause();
        LogHelper.i();
    }

    @Override
    protected void onStop() {
        super.onStop();
        LogHelper.i();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LogHelper.i();
    }

//    1 onPause
//    2 onCreate(finish)
//          2 onStart
//          2 onResume
//          1 onStop

//          2 onPause
//          2 onStop
//          1 onStart
//    1 onResume
//    2 onDestroy






//    1 onDestroy
}


