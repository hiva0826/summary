package com.hiva.wms

import android.app.Activity
import android.os.Bundle
import android.view.View
import com.hiva.helper.file.FileIOUtils
import com.hiva.helper.log.LogHelper
import com.hiva.wms.bean.People
import com.hiva.wms.button.FloatButton
import com.hiva.wms.databinding.ActivityWmsBinding
import java.io.File

/**
 *
 * Create By HuangXiangXiang 2022/12/30
 */
class WmsActivity : Activity(), View.OnClickListener {

    private lateinit var binding : ActivityWmsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityWmsBinding.inflate(layoutInflater)
        binding.startCamera.setOnClickListener(this)
        binding.saveBtn.setOnClickListener(this)
        binding.loadBtn.setOnClickListener(this)

        setContentView(binding.root)

        floatButton = FloatButton(this)
    }

    private lateinit var  data: ByteArray

    private lateinit var floatButton: FloatButton

    override fun onClick(v: View?) {

        if(v == binding.startCamera){

            floatButton.showView()

//            val intent = Intent(this, WmsService::class.java)
//            startService(intent)

        }else if(v == binding.saveBtn) {

            val people = People("张三丰", 99, true)
            val data = ParcelUtils.parcelToByteArray(people)

            val file = File(getExternalFilesDir("people"), "people")
            FileIOUtils.writeBytes(file,data)

        }else if(v == binding.loadBtn) {

            val file = File(getExternalFilesDir("people"), "people")
            val data = FileIOUtils.readBytes(file)
            val people = ParcelUtils.byteArrayToParcel(data)
            LogHelper.i(people)

        }
    }




}