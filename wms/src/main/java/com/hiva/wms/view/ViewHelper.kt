package com.hiva.wms.view

import android.content.Context
import android.view.View
import android.view.WindowManager

/**
 *
 * Create By HuangXiangXiang 2022/12/30
 */
object ViewHelper {

    /**
     * 添加view
     * */
    fun addView(context: Context, view: View){
        val windowManager: WindowManager =
            context.getSystemService(Context.WINDOW_SERVICE) as WindowManager

        val params = WindowManager.LayoutParams()
        params.width = 1
        params.height = 1
        params.alpha = 0f
        params.type = WindowManager.LayoutParams.TYPE_SYSTEM_ALERT
//        params.type = WindowManager.LayoutParams.TYPE_TOAST

        // 屏蔽点击事件
        params.flags = (WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                or WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                or WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)

        windowManager.addView(view, params)
    }


    /**
     * 移除view
     * */
    fun removeView(context: Context, view: View){

        val windowManager: WindowManager =
            context.getSystemService(Context.WINDOW_SERVICE) as WindowManager


        windowManager.removeView(view)
    }



}