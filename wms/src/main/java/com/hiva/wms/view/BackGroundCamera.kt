package com.hiva.wms.view

import android.content.Context
import android.hardware.Camera
import com.hiva.helper.file.FileIOUtils
import java.io.File

/**
 *
 * Create By HuangXiangXiang 2022/12/30
 *
 * 后台摄像头
 */
object BackGroundCamera {
    /**
     * 获取可用的摄像头
     */
    private fun getCanUsedCamera(): Camera? {

        var camera: Camera? = null
        try {
            camera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK) //打开后置摄像头
        } catch (e: Exception) {
        }

        if (camera == null) {
            try {
                camera = Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT) //打开前置摄像头
            } catch (e: Exception) {
            }
        }
        return camera
    }

    fun startTakePic(context : Context) {
        val camera = getCanUsedCamera() ?: return





//            val preview = SurfaceView(context)
//            ViewHelper.addView(context, preview)

//            camera!!.setPreviewDisplay(preview.holder)

        val jpeg = Camera.PictureCallback(){
                bytes: ByteArray, camera: Camera ->

            val file = File(context.getExternalFilesDir("image"),System.currentTimeMillis().toString() + ".jpg")
            FileIOUtils.writeBytes(file, bytes)

            camera.stopPreview()
            camera.release()
        }
        camera.takePicture(null, null, jpeg)
    }


}