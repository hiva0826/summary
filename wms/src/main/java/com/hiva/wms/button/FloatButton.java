package com.hiva.wms.button;

import android.content.Context;
import android.graphics.PixelFormat;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;

import com.hiva.helper.log.LogHelper;

public class FloatButton {

    // 全局上下文
    private final WindowManager windowManager;
    private final View monitorView;
    private final WindowManager.LayoutParams layoutParams;

    public FloatButton(Context context) {

        this.windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        this.monitorView = new MonitorView(context);
        this.layoutParams = new WindowManager.LayoutParams(
                1,1,
                WindowManager.LayoutParams.TYPE_TOAST,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                PixelFormat.TRANSLUCENT
        );
        showView();
    }


    private static class MonitorView extends View{
        public MonitorView(Context context) {
            super(context);
        }

        @Override
        public boolean dispatchKeyEvent(KeyEvent event) {

            LogHelper.i(event);
            if(event.getAction() == KeyEvent.ACTION_DOWN){
                if(event.getKeyCode() == 276){
                    LogHelper.w(event);
//                    return true;
                }
            }

            return true;
//            return super.dispatchKeyEvent(event);
        }
    }


    private boolean isAdd = false;
    /**
     * 显示悬浮按钮
     */
    public void showView() {

        LogHelper.i(isAdd);
        if(!isAdd){
            isAdd = true;
            windowManager.addView(monitorView, layoutParams);
        }
    }


    /**
     * 隐藏悬浮按钮
     */
    public void hideView() {

        LogHelper.i(isAdd);
        if(isAdd){
            isAdd = false;
            windowManager.removeView(monitorView);
        }

    }






}

