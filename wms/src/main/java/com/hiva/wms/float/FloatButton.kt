//package com.hiva.wms.float
//
//import android.content.Context
//import android.graphics.Color
//import android.graphics.PixelFormat
//import android.graphics.Rect
//import android.view.*
//import android.view.GestureDetector.SimpleOnGestureListener
//import android.widget.Button
//import androidx.core.view.GestureDetectorCompat
//import androidx.core.view.MotionEventCompat
//import com.hiva.helper.app.PxUtils
//import com.hiva.helper.log.LogHelper
//import com.hiva.wms.R
//
///**
// *
// * Create By HuangXiangXiang 2022/12/30
// */
//class FloatButton(
//    private var context: Context,
//    private var listener: View.OnClickListener
//    ) {
//
//    // 悬浮按钮相关
//    private var windowManager: WindowManager? = null
//    private var mLayoutParams: WindowManager.LayoutParams? = null
//    private var floatBtnView: Button? = null
//
//    // 悬浮按钮的宽和高
//    private var FIX_LENGTH = 0
//
//    /**
//     * 手势监听
//     */
//    private var mDetector: GestureDetectorCompat? = null
//
//    private var screenWidth = 0
//    private var screenHalfWidth = 0
//
//    init {
//        FIX_LENGTH = PxUtils.dp2px(context, 40)
//        val metrics = context.resources.displayMetrics
//        screenWidth = metrics.widthPixels
//        screenHalfWidth = screenWidth / 2
//        init()
//        showView()
//    }
//
//    /**
//     * 创建悬浮按钮
//     *
//     * @param context
//     * @param wmParams
//     * @param resId
//     * @return
//     */
//    private fun createFloatView(
//        context: Context?,
//        wmParams: WindowManager.LayoutParams,
//        resId: Int
//    ): Button? {
//        val localMyFloatView = Button(context)
//        localMyFloatView.textSize = 8f
//        localMyFloatView.text = "锁屏"
//        localMyFloatView.setTextColor(Color.BLACK)
//        localMyFloatView.setBackgroundResource(resId)
//        //WindowManager localWindowManager = (WindowManager) context.getSystemService("window");
//
//        // 设置window type 这里是关键，你也可以试试2003
////        wmParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_ALERT;
//        wmParams.type = WindowManager.LayoutParams.TYPE_PHONE
//        //wmParams.type = WindowManager.LayoutParams.TYPE_TOAST;
//
//        // 设置图片格式，效果为背景透明
//        wmParams.format = PixelFormat.TRANSPARENT
//        wmParams.format = PixelFormat.RGBA_8888
//
//        // http://www.jcodecraeer.com/a/anzhuokaifa/androidkaifa/2012/1105/509.html
//        // 设置浮动窗口不可聚焦（实现操作除浮动窗口外的其他可见窗口的操作）
//        wmParams.flags = wmParams.flags or (WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
//                or WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
//                or WindowManager.LayoutParams.FLAG_LAYOUT_INSET_DECOR
//                or WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN)
//        /*
//        wmParams.flags |=
//                        WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL |
//                        WindowManager.LayoutParams.FLAG_IGNORE_CHEEK_PRESSES;
//                        */
//        //DisplayMetrics metrics = getMetrics(context);
//        // 以屏幕左上角为原点，设置x、y初始值，相对于gravity
//        wmParams.x = PxUtils.dp2px(context, 4)
//        wmParams.y = PxUtils.dp2px(context, 4)
//
//        // 设置悬浮窗口长宽数据
//        // wmParams.width = WindowManager.LayoutParams.WRAP_CONTENT;
//        // wmParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
////        try {
////            wmParams.width = Integer.parseInt(MyConfigure.getValue("float_width"));
////            wmParams.height = Integer.parseInt(MyConfigure.getValue("float_height"));
////            LogUtils.e(TAG, "createFloatView " + wmParams.width + "*" + wmParams.height);
////        } catch (NumberFormatException e) {
////            e.printStackTrace();
////        }
//
//        //localWindowManager.addView(localMyFloatView, wmParams);
//        localMyFloatView.isClickable = true
//
//        //showFloatView(context,localMyFloatView,wmParams);
//        return localMyFloatView
//    }
//
//    /**
//     * 初始化
//     */
//    private fun init() {
//
//
//        /**
//         * 手势监听
//         */
//        val gestureListener: GestureDetector.OnGestureListener =
//            object : SimpleOnGestureListener() {
//                private var lastX = 0f
//                private var lastY = 0f
//                private var originX = 0f
//                private var originY = 0f
//
//                /**
//                 * 单击按下
//                 * @param e
//                 * @return
//                 */
//                override fun onDown(e: MotionEvent): Boolean {
//                    //获取到状态栏的高度
//                    val frame = Rect()
//                    floatBtnView!!.getWindowVisibleDisplayFrame(frame)
//                    //            LogHelper.e(frame.height());
//                    lastX = e.rawX // 获取触摸事件触摸位置的原始X坐标
//                    lastY = e.rawY
//                    originX = lastX
//                    originY = lastY
//                    return super.onDown(e)
//                }
//
//                /**
//                 * 确定了，就是单击
//                 * @param e
//                 * @return
//                 */
//                override fun onSingleTapConfirmed(e: MotionEvent): Boolean {
////            LogHelper.e();
//                    listener?.onClick(null)
//                    return super.onSingleTapConfirmed(e)
//                }
//
//                /**
//                 * 滚动
//                 * @param e1
//                 * @param e2
//                 * @param distanceX
//                 * @param distanceY
//                 * @return
//                 */
//                override fun onScroll(
//                    e1: MotionEvent,
//                    e2: MotionEvent,
//                    distanceX: Float,
//                    distanceY: Float
//                ): Boolean {
//                    val dx = e2.rawX - lastX
//                    val dy = e2.rawY - lastY
//                    updateViewPosition(dx, dy)
//                    lastX = e2.rawX
//                    lastY = e2.rawY
//                    LogHelper.e("$dx, $dy ： $lastX, $lastY")
//                    floatBtnView!!.setBackgroundResource(R.drawable.float_button_moving)
//                    return super.onScroll(e1, e2, distanceX, distanceY)
//                }
//
//                /**
//                 * 双击
//                 * @param e
//                 * @return
//                 */
//                override fun onDoubleTap(e: MotionEvent): Boolean {
////            LogHelper.e();
//                    listener?.onClick(null)
//                    return true
//                }
//
//                /**
//                 * 长按
//                 * @param e
//                 */
//                override fun onLongPress(e: MotionEvent) {
////            LogHelper.e();
//                    listener?.onClick(null)
//                }
//            }
//
//
//        // 手势监控
//        mDetector = GestureDetectorCompat(context, gestureListener)
//        // 构造悬浮按钮
//        windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
//        mLayoutParams = WindowManager.LayoutParams()
//        floatBtnView = createFloatView(context, mLayoutParams!!, R.drawable.selector_float_btn)
//        mLayoutParams!!.width = FIX_LENGTH
//        mLayoutParams!!.height = FIX_LENGTH
//        mLayoutParams!!.gravity = Gravity.LEFT
//        mLayoutParams!!.alpha = 0.5f
//        floatBtnView!!.setOnTouchListener { v, event ->
//            val action: Int = MotionEventCompat.getActionMasked(event)
//            when (action) {
//                MotionEvent.ACTION_DOWN -> {
//                }
//                MotionEvent.ACTION_MOVE -> {
//                }
//                MotionEvent.ACTION_UP -> {
//                    floatBtnView!!.setBackgroundResource(R.drawable.selector_float_btn)
//                    if (event.rawX <= screenHalfWidth) {
//                        mLayoutParams!!.x = 0
//                    } else {
//                        mLayoutParams!!.x = screenWidth
//                    }
//                    windowManager!!.updateViewLayout(floatBtnView, mLayoutParams)
//                }
//                MotionEvent.ACTION_POINTER_DOWN -> {
//                }
//                MotionEvent.ACTION_POINTER_UP -> {
//                }
//                MotionEvent.ACTION_CANCEL -> {
//                }
//            }
//            mDetector?.onTouchEvent(event)?:false
//        }
//    }
//
//    private var isAdd = false
//
//    /**
//     * 显示悬浮按钮
//     */
//    fun showView() {
//        if (!isAdd) {
//            windowManager!!.addView(floatBtnView, mLayoutParams)
//            mLayoutParams!!.x = screenWidth
//            windowManager!!.updateViewLayout(floatBtnView, mLayoutParams)
//            isAdd = true
//        }
//        floatBtnView!!.visibility = View.VISIBLE
//        floatBtnView!!.isEnabled = true
//    }
//
//
//    /**
//     * 隐藏悬浮按钮
//     */
//    fun hideView() {
//        if (isAdd) {
//            windowManager!!.removeView(floatBtnView)
//            isAdd = false
//        }
//        floatBtnView!!.visibility = View.GONE
//        floatBtnView!!.isEnabled = false
//    }
//
//
//
//
//    /**
//     * 更新坐标
//     * @param x
//     * @param y
//     */
//    private fun updateViewPosition(x: Float, y: Float) {
//        mLayoutParams!!.x += x.toInt()
//        mLayoutParams!!.y += y.toInt()
//        windowManager!!.updateViewLayout(floatBtnView, mLayoutParams)
//    }
//
//
//}