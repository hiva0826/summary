package com.hiva.wms

import android.os.Parcel
import android.os.Parcelable

object ParcelUtils {



    /**
     * 将 Parcelable 转换成byte数组
     * */
    fun parcelToByteArray(parcelable: Parcelable?): ByteArray {

        val parcel = Parcel.obtain()
        parcel.writeParcelable(parcelable, 0)
        val data = parcel.marshall()
        parcel.recycle()

        return data
    }

    /**
     * 将 byte数组 转换成 T
     * */
    fun <T : Parcelable> byteArrayToParcel(byteArray: ByteArray, tClass: Class<T>): T? {

        val parcel = Parcel.obtain()
        parcel.unmarshall(byteArray, 0, byteArray.size)
        parcel.setDataPosition(0)
        val parcelable = parcel.readParcelable<T>(tClass.classLoader)
        parcel.recycle()

        return parcelable
    }

    /**
     * 将 byte数组 转换成 Parcelable
     * */
    fun byteArrayToParcel(byteArray: ByteArray): Parcelable? {

        val parcel = Parcel.obtain()
        parcel.unmarshall(byteArray, 0, byteArray.size)
        parcel.setDataPosition(0)
        val parcelable = parcel.readParcelable<Parcelable>(javaClass.classLoader)
        parcel.recycle()

        return parcelable
    }







}