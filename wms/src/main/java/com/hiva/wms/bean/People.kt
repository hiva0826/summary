package com.hiva.wms.bean

import android.os.Parcel
import android.os.Parcelable

/**
 *
 * Create By HuangXiangXiang 2023/1/4
 */
data class People(
    val name : String?,
    val age : Int,
    val sex : Boolean,
) : Parcelable {
    
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readInt(),
        parcel.readByte() != 0.toByte()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeInt(age)
        parcel.writeByte(if (sex) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<People> {
        override fun createFromParcel(parcel: Parcel): People {
            return People(parcel)
        }

        override fun newArray(size: Int): Array<People?> {
            return arrayOfNulls(size)
        }
    }
}
