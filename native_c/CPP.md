#### C java类签名
| Java类型  | 类型签名    |
|---------|---------|
| boolean | Z       |
| byte    | B       |
| short   | S       |
| int     | I       |
| char    | C       |
| long    | J       |
| float   | F       |
| double  | D       |
| 类       | L全限定类名; |
| 数组      | [元素类型签名 |


#### C语言java方法签名
| Java类型                          | 类型签名                                                  |
|---------------------------------|-------------------------------------------------------|
| String f()                      | ()Ljava/lang/String;                                  |
| long f(int i, Class c)          | (ILjava/lang/Class;)J                                 |
| String f(String s, int i)       | (Ljava/lang/String;I)Ljava/lang/String;               |
| void f(byte[] bytes)            | ([B)V                                                 |
| Context[] f(Context c)          | (Landroid/content/Context;)[Landroid/content/Context; |
| ArrayList<Context> f(Context c) | (Landroid/content/Context;)Ljava.util.ArrayList;      |
