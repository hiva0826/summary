#include <string.h>
#include <stdio.h>
#include <malloc.h>

#include <pthread.h>

/**
 * 获取int 数组长度
 * */
int getIntLength(int age){

    int length;
    if(age < 0){
        length = 2;
        age = -age;
    }else{
        length = 1;
    }

    while (1){
        age = age/10;
        if(age <= 0){
            return length;
        }
        length++ ;
    }
}


///**
// * 添加符数据值
// * 需要释放
// * */
//char* appendChar(const char* first, const char* second){
//
//    int firstLength = strlen(first);
//    int secondLength = strlen(second);
//    int length = firstLength + secondLength;
//    char* result = malloc(length * sizeof(char)); // 动态创建内存
//    strcpy(result, first);
//    strcpy(result + firstLength, second);
//    return result;
//}

//int main(void)
//{
//
//
//    const char* first = "张三" ;  // 获取数据
//    const char* second = "1989" ; //后面数据
//    char* description = appendChar(first, second);
//    printf("%s" ,description );
//    free(description);
//    return 0;
//}
///**
// * 修改字符数据值
// *
// * data 数据的指针
// * */
//void changeCharValue(const char* *data, char* value){
//
//    const char* first = *data;  // 获取数据
//    const char* second = "1989" ; //后面数据
//
//    int firstLength = strlen(first);
//    int secondLength = strlen(second);
//
//    char result[100];
////    extern char result[firstLength + secondLength];
//    strcpy(result, first);
//    strcpy(result + firstLength, second);
//
//    *data = result; // 修改数据
//}


/**
 * 修改数据值
 *
 * data 数据的指针
 *
 * */
void changeIntValue(int* data){

    int value = *data; //获取data的数据
    value ++; //数据修改（加+1）
    *data = value; //向指针设置数据
}


int getMax(int first, int second){
    if(first > second){
        return first;
    }
    return second;
}




