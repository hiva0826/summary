#include <iostream>
using namespace std;

class String
{
    public:
        const char *name;
        String(const char *name){
            this->name = name;
            printf("构造函数\n");
        }

        ~String(){
            printf("析构函数\n");
            delete name;
        }

        String operator+(String other){

            int firstLength = strlen(name);
            int secondLength = strlen(other.name);
            if(secondLength > 0){

                int newSize = (firstLength + secondLength) * sizeof(char);
                char *result = new char [newSize];

                strcpy(result , name);
                strcpy(result + firstLength, other.name);

                return result;
            }

            return this->name;
        }
};









