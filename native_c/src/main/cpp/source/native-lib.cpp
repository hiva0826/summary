#include <jni.h>
#include <string>
#include <iostream>

#include "first.c"
#include "second.cpp"

using namespace std;

extern "C"
JNIEXPORT jstring JNICALL Java_com_hiva_native_1c_TestC_getNativeResult1(JNIEnv *env, jclass clazz, jstring name, jint age) {

    const char *first = env->GetStringUTFChars(name, nullptr);
    size_t nameLength = strlen(first);
    size_t ageLength = getIntLength(age);

    char* result = new char [1+nameLength+1+ageLength+1];
    int start = 0;
    int length = 1;
    snprintf(result + start ,length + 1 , "%s" ,"(" );//(
    start += length;
    length = nameLength;
    snprintf(result + start ,length + 1 , "%s" ,first );// name
    start += length;
    length = 1;
    snprintf(result + start ,length + 1, "%s" ,"," );// ,
    start += length;
    length = ageLength;
    snprintf(result + start , length + 1, "%d" , age );//age
    start += length;
    length = 1;
    snprintf(result + start ,length + 1, "%s" ,")" );// )

    return env->NewStringUTF(result);
}



extern "C"
JNIEXPORT jstring JNICALL
Java_com_hiva_native_1c_TestC_getNativeResult2(JNIEnv *env, jclass clazz, jstring name, jint age) {

    string first = env->GetStringUTFChars(name, nullptr);
    string second = to_string(age);
    string result = "("+ first + ","+ second + ")";

    return env->NewStringUTF(result.c_str());
}
extern "C"
JNIEXPORT jstring JNICALL Java_com_hiva_native_1c_TestC_getPackageName(JNIEnv *env, jclass clazz, jobject context) {

    jclass contextClass = env->FindClass("android/content/Context");
    const char* name = "getPackageName" ;
    const char* sig = "()Ljava/lang/String;";

    jmethodID getPackageNameMethod = env->GetMethodID(contextClass, name , sig);
    if(getPackageNameMethod == nullptr){
        return nullptr;
    }
    jobject result = env->CallObjectMethod(context, getPackageNameMethod);

    return static_cast<jstring>(result);
}
extern "C"
JNIEXPORT jstring JNICALL
Java_com_hiva_native_1c_TestC_getNativeResult3(JNIEnv *env, jclass clazz, jstring name, jint age) {

    jclass funClazz = env->FindClass("com/hiva/native_c/TestC");
    const char* methodName = "getResult" ;
    const char* signe = "(Ljava/lang/String;I)Ljava/lang/String;";

    jmethodID getPackageNameMethod = env->GetStaticMethodID(funClazz, methodName , signe);
    if(getPackageNameMethod == nullptr){
        return nullptr;
    }
    jobject result = env->CallStaticObjectMethod(funClazz, getPackageNameMethod, name, age);

    return static_cast<jstring>(result);
}
extern "C"
JNIEXPORT jstring JNICALL
Java_com_hiva_native_1c_TestC_getSystemUpdateService(JNIEnv *env, jclass clazz) {

    jclass funClazz = env->FindClass("android/content/Context");
    const char* name = "SYSTEM_UPDATE_SERVICE" ;
//    const char* name = "POWER_SERVICE" ;
    const char* signe = "Ljava/lang/String;";

    jfieldID fieldId = env->GetStaticFieldID(funClazz, name , signe);
    if(fieldId == nullptr){
        return nullptr;
    }

    jobject result = env->GetStaticObjectField(funClazz, fieldId);

    return static_cast<jstring>(result);

}

extern "C"
JNIEXPORT jobject JNICALL
Java_com_hiva_native_1c_TestC_test(JNIEnv *env, jclass clazz, jobjectArray parameter) {

    return nullptr;
}


struct People{

    const char *name;
    int age;

    void changeValue(){

        const char* first = name;  // 获取数据
        const char* second = "1989" ; //后面数据
        int firstLength = strlen(first);
        int secondLength = strlen(second);
        char result[firstLength + secondLength];
        strcpy(result, first);
        strcpy(result + firstLength, second);
        name = result;

        changeIntValue(&age);

    }

};


void changeCharValue(const char **pString, const char *name);

void *testResponse();

People transformCPeople(JNIEnv *env, jclass peopleClass, jobject jPeople){

    People cPeople{};
    jfieldID nameFieldID = env->GetFieldID(peopleClass, "name", "Ljava/lang/String;") ;
    jstring name = static_cast<jstring>(env->GetObjectField(jPeople, nameFieldID));
    cPeople.name = env->GetStringUTFChars(name, nullptr);

    jfieldID ageFieldID = env->GetFieldID(peopleClass, "age", "I") ;
    cPeople.age = env->GetIntField(jPeople, ageFieldID);

    return cPeople;
}

jobject transformJPeople(JNIEnv *env, jclass peopleClass, People cPeople){

    jstring name = env->NewStringUTF(cPeople.name);
    jint age = cPeople.age;

    jmethodID initMethodID = env->GetMethodID(peopleClass, "<init>" ,"(Ljava/lang/String;I)V");
    return env->NewObject(peopleClass, initMethodID, name, age);
}


extern "C"
JNIEXPORT jobject JNICALL
Java_com_hiva_native_1c_TestC_changePeople(JNIEnv *env, jclass clazz, jobject people) {

    jclass peopleClass = env->GetObjectClass(people);
    People cPeople = transformCPeople(env, peopleClass, people);

    jobject jPeople = transformJPeople(env, peopleClass, cPeople);

    return jPeople;
}



/**
 * 排序
 * */
extern "C"
JNIEXPORT jobject JNICALL
Java_com_hiva_native_1c_TestC_sortPeople(JNIEnv *env, jclass clazz, jobject peoples) {

    int (*p)(int, int) = &getMax;
    int a,b;
    int d = p(a,b);
    return nullptr;
}


extern "C"
JNIEXPORT jint JNICALL
Java_com_hiva_native_1c_TestC_getMax(JNIEnv *env, jclass clazz, jint first, jint second) {

    int (*p)(int, int) = &getMax;
    return p(first, second);
}


extern "C"
JNIEXPORT jstring JNICALL
Java_com_hiva_native_1c_TestC_readFile(JNIEnv *env, jclass clazz, jstring path) {

    const char* fileName = env->GetStringUTFChars(path, nullptr) ;
    const FILE *file = fopen(fileName, "r") ;
    if(file != nullptr){
        int length = 255;
        char buff[length];

        fgets(buff, length, (FILE*)file);

        return env->NewStringUTF(buff);
    }

    return nullptr;
}


void response(JNIEnv *env, jobject listener){

    jclass clazz = env->GetObjectClass(listener);
    jmethodID methodId = env->GetMethodID(clazz, "onResponse","()V") ;
    env->CallVoidMethod(listener, methodId);
}

JNIEnv *jniEnv;
jobject listener;

void *test(void * arg)
{
    response(jniEnv, listener);

    return nullptr;
}

extern "C"
JNIEXPORT void JNICALL
Java_com_hiva_native_1c_TestC_response(JNIEnv *env, jclass clazz, jobject onResponseListener) {

//    jniEnv = env;
//    listener = onResponseListener;
//
//    pthread_t tid;
//    pthread_create(&tid, nullptr , test, nullptr);
//    test(nullptr);

    response(env, onResponseListener);
}

extern "C"
JNIEXPORT jlong JNICALL
Java_com_hiva_native_1c_TestC_getAddress(JNIEnv *env, jclass clazz) {

    int* value ;
    value = new int[10];
    value[0] = 1024;

    int*p = value;
    long address = reinterpret_cast<long>(p);

    return address;
}

extern "C"
JNIEXPORT jint JNICALL
Java_com_hiva_native_1c_TestC_getValue(JNIEnv *env, jclass clazz, jlong address) {

    int *p = reinterpret_cast<int *>(address);
    int value = *p;

    return value ;
}