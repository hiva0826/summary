package com.hiva.native_c;


import android.content.Context;

import com.hiva.helper.log.LogHelper;

import java.util.ArrayList;
import java.util.Locale;

public class TestC {

    // Used to load the 'nativec' library on application startup.
    static {
        System.loadLibrary("nativec");
    }

    public static native String getNativeResult1(String name, int age);

    public static native String getNativeResult2(String name, int age);

    public static native String getPackageName(Context context);

    public static native String getNativeResult3(String name, int age);

    public static native String getSystemUpdateService();

    public static native Object test(Object... parameter);

    public static native People changePeople(People peoples);

    private static native ArrayList<People> sortPeople(ArrayList<People> peoples);

    public static native int getMax(int first, int second);

    public static native String readFile(String path);


    public interface OnResponseListener{
        void onResponse();
    }
    public static native void response(OnResponseListener listener);

    /**
     * 获取地址
     * */
    public static native long getAddress();

    /**
     * 获取数据值
     * */
    public static native int getValue(long address);


//    private static ArrayList<People> sortPeople(ArrayList<People> peoples){
//
//        Collections.sort(peoples, new Comparator<People>() {
//            @Override
//            public int compare(People o1, People o2) {
//                return o1.age - o2.age;
//            }
//        });
//
//        return peoples;
//    }


    public static String getResult(String name, int age) {
        return String.format(Locale.CHINA, "(%s, %d) " ,name, age);
    }

    public static void sortPeople(){

        ArrayList<People> peoples = new ArrayList<>();

        peoples.add(new People("张三", 39)) ;
        peoples.add(new People("李四", 48)) ;
        peoples.add(new People("王二", 28)) ;
        peoples.add(new People("赵六", 68)) ;

        peoples = sortPeople(peoples);

        LogHelper.i(peoples);
    }


    public static class People{

        private final String name;
        private final int age;

        public People(String name, int age) {
            this.name = name;
            this.age = age;
        }

        @Override
        public String toString() {
            return String.format(Locale.CHINA, "(%s,%d)", name, age);
        }
    }

    public static class MyInt{

        public int value;

    }

    public static void changeValue(MyInt first, MyInt second){

        int temp = first.value; //获取地址的数据
        second.value = first.value;// 设置数据
        first.value = temp;
    }
}