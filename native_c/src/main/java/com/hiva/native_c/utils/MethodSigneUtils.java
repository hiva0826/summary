package com.hiva.native_c.utils;


import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class MethodSigneUtils {




    private static class SigneInfo{
        /**类型*/
        private final Class<?> clazz;
        /**签名*/
        private final String signe;
        private SigneInfo(Class<?> clazz, String signe) {
            this.clazz = clazz;
            this.signe = signe;
        }
    }

    /**
     * 基本类型 java signe
     * */
    private static final Collection<SigneInfo> signeInfos = new ArrayList<>();
    static {
        signeInfos.add(new SigneInfo(boolean.class, "Z"));
        signeInfos.add(new SigneInfo(byte.class, "B"));
        signeInfos.add(new SigneInfo(short.class, "S"));
        signeInfos.add(new SigneInfo(int.class, "I"));
        signeInfos.add(new SigneInfo(char.class, "C"));
        signeInfos.add(new SigneInfo(long.class, "J"));
        signeInfos.add(new SigneInfo(float.class, "F"));
        signeInfos.add(new SigneInfo(double.class, "D"));
        signeInfos.add(new SigneInfo(Void.class, "V"));
    }


    /**
     * 基本类型 java signe
     * */
    private static final HashMap<Class<?>, String> basicJavaSigne = new HashMap<>();
    //
    private static final HashMap<String, Class<?>> basicSigneJava = new HashMap<>();
    static {

        for (SigneInfo signeInfo : signeInfos) {

            basicJavaSigne.put(signeInfo.clazz, signeInfo.signe);
            basicSigneJava.put(signeInfo.signe, signeInfo.clazz);
        }
    }


    private static String getSigne(Class<?> clazz){

        String signe = basicJavaSigne.get(clazz);
        if(signe != null){
            return signe;
        }

        signe = getArraySigne(clazz);
        if(signe != null){
            return signe;
        }

        return getNormalSigne(clazz) ;
    }

    /**
     * 数组
     * */
    private static String getArraySigne(Class<?> clazz){

        String signe = clazz.getName();
        if(signe.startsWith("[")){
            return signe;
        }
        return null;
    }

    /**
     * 普通类的签名
     * L + 类路径（/替换.） + ;
     * */
    private static String getNormalSigne(Class<?> clazz){

        return 'L'+ clazz.getName() +  ';';
    }


    /**
     * 根据方法获取签名
     * */
    public static String getSigne(Method method){

        String signe = getSigne1(method);
        if(signe != null){
            return signe;
        }
        return getSigne2(method);
    }

    /**
     * 使用反射直接调用原生
     * */
    public static String getSigne1(Method method) {

        try {
            Method getSignatureMethod = Method.class.getDeclaredMethod("getSignature" );
            getSignatureMethod.setAccessible(true);
            String signe = (String) getSignatureMethod.invoke(method);
            return signe.replace('.', '/');
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return null ;
    }


    public static String getSigne2(Method method){

        StringBuilder sb = new StringBuilder();
        sb.append("(") ;
        // 请求参数
        Class<?>[] parameterTypes = method.getParameterTypes();
        if(parameterTypes != null && parameterTypes.length > 0){
            for (Class<?> parameterType : parameterTypes) {
                sb.append(getSigne(parameterType));
            }
        }
        sb.append(")") ;

        // 返回参数
        Class<?> returnType = method.getReturnType() ;
        sb.append(getSigne(returnType));

        return sb.toString().replace('.', '/');
    }





    /**
     * 根据签名获取方法
     * */
    public static Method getMethod(Class<?> clazz , String methodName, String signe){


        return null;
    }
}
