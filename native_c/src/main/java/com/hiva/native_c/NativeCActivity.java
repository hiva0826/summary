package com.hiva.native_c;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.hiva.helper.log.LogHelper;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Locale;

/**
 * Create By HuangXiangXiang 2022/12/15
 */
public class NativeCActivity extends Activity {

    private TextView mResultTvw ;
//    private final TestC testC = new TestC();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nativec);

        mResultTvw = (TextView)findViewById(R.id.result_tvw) ;
    }

    int age = 10;
    public void getJniData(View view){


        long address = TestC.getAddress();
        LogHelper.i(String.format(Locale.CHINA, "%X", address));

        int value = TestC.getValue(address);
        LogHelper.i(String.format(Locale.CHINA, "%d", value));


//        TestC.sortPeople();

//        String path = new File(getExternalFilesDir("temp"), "message.txt").getPath();
//        LogHelper.i(path);
//        String result = TestC.readFile(path);
//        LogHelper.i(result);

//        TestC.People p = new TestC.People("张三", 39);
//        TestC.People p2 = TestC.changePeople(p);
//
//        LogHelper.i(p2 == p);
//        LogHelper.i(p2);
    }

    private int value;
    public void original(View view){

        f(null,1) ;
    }

    public String f(String name, int age){

//        Object obj = new Object(){};
//        Method method = obj.getClass().getEnclosingMethod();
//        mResultTvw.setText(MethodSigneUtils.getSigne(method));
//
//        try {
//            Method getSignatureMethod = Method.class.getDeclaredMethod("getSignature" );
//            getSignatureMethod.setAccessible(true);
//            mResultTvw.append("\n" + getSignatureMethod.invoke(method).toString().replace('.', '/'));
//        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
//            e.printStackTrace();
//        }

        return null;
    }

}
