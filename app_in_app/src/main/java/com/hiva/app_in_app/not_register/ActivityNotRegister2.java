package com.hiva.clazz.not_register;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Create By HuangXiangXiang 2022/12/9
 *
 * VERSION.SDK_INT  == [26 27]
 *
 */
public class ActivityNotRegister2 extends ActivityNotRegister {

    @Override
    protected Result onInitToAMS() {

        try {
            Class<?> cls = Class.forName("android.app.ActivityManager");
            Field field = cls.getDeclaredField("IActivityManagerSingleton") ;
            field.setAccessible(true);
            Object iActivityManagerSingleton = field.get(null) ;  // 获取 Singleton 实例 IActivityManagerSingleton
            cls = Class.forName("android.util.Singleton");

            Method getMethod = cls.getMethod("get"); // 防止第一次获取为空
            getMethod.invoke(iActivityManagerSingleton);

            field = cls.getDeclaredField("mInstance") ;
            field.setAccessible(true);

            return new Result(iActivityManagerSingleton, field);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }



}
