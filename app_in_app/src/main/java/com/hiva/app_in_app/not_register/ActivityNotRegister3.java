package com.hiva.clazz.not_register;

import android.content.Intent;
import android.os.Message;

import com.hiva.helper.clazz.ClassUtils;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Create By HuangXiangXiang 2022/12/9
 *
 * VERSION.SDK_INT  == [28]
 *
 */
public class ActivityNotRegister3 extends ActivityNotRegister2 {

    @Override
    protected Intent onFindIntentFromFromAMS(Message msg) {

        if(msg.what == 159){
            try {

                Class<?> cls = Class.forName("android.app.servertransaction.ClientTransaction") ;
                Field field = cls.getDeclaredField("mActivityCallbacks") ;
                field.setAccessible(true);

                List<?> list = (List<?>) field.get(msg.obj);
                if(list != null && !list.isEmpty()){
                    Object object = list.get(0) ;
                    cls = Class.forName("android.app.servertransaction.LaunchActivityItem") ;
                    if(ClassUtils.isSonClass2(cls, object.getClass())){

                        field = cls.getDeclaredField("mIntent") ;
                        field.setAccessible(true);
                        return (Intent) field.get(object);
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }

        }

        return null;
    }
}
