package com.hiva.clazz.not_register;

import android.content.Intent;

import java.lang.reflect.Method;

/**
 * Create By HuangXiangXiang 2022/12/9
 *
 * VERSION.SDK_INT  == [30]
 *
 */
public class ActivityNotRegister5 extends ActivityNotRegister4 {

    @Override
    protected Intent onFindIntentFromToAMS(Method method, Object[] args) {

        if("startActivity".equals(method.getName())){
            return (Intent) args[3];
        }
        return null;
    }


}
