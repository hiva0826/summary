package com.hiva.clazz.utils;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;

import com.hiva.clazz.UninstallApp;
import com.hiva.clazz.not_register.ActivityNotRegister;
import com.hiva.clazz.not_register.ActivityNotRegister2;
import com.hiva.clazz.not_register.ActivityNotRegister3;
import com.hiva.clazz.not_register.ActivityNotRegister4;
import com.hiva.clazz.not_register.ActivityNotRegister5;
import com.hiva.helper.file.CopyAssetFileUtils;
import com.hiva.helper.log.LogHelper;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;

import dalvik.system.DexClassLoader;

/**
 * Create By HuangXiangXiang 2023/1/5
 */
public class ApkUtils {

    public static ArrayList<UninstallApp> loadUninstallApps(Context context){

        activityNotRegister(context);// 使用未注册的Activity

        ArrayList<UninstallApp> apps = new ArrayList<>();

        File des = context.getExternalFilesDir("apk");
        CopyAssetFileUtils.copy(context, "apk", des);

        File[] files =des.listFiles() ;
        if(files != null){

            for (File f : files) {
                UninstallApp app = loadUninstallApp(context, f.getPath());
                if(app != null){
                    apps.add(app);
                }
            }
        }
        return apps;
    }

    private static UninstallApp loadUninstallApp(Context context, String path) {

        String className = getFirstClassName(path);
        if(className != null){
            UninstallApp app = new UninstallApp();
            app.setFirstClassName(className);
            app.setPath(path);

            app.setLabel(new File(path).getName());
            return app;
        }
        return null;
    }

    public static void startApK(Context context, UninstallApp app){

        HotFixResourceUtils.hotFixResource(context, app.getPath()); // 热修复使用资源
        changeDexOrder(context, app.getPath()); // 更改加载dex 顺序

        Intent intent = new Intent();
        String className = app.getFirstClassName() ;
        if(className != null){

            try {
                Class<?> clazz = Class.forName(className);
                intent.setClassName(context, className);
                context.startActivity(intent);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 不用注册Activity就能使用
     * */
    private static void activityNotRegister(Context context){

        final ActivityNotRegister register ;
        int sdk = Build.VERSION.SDK_INT;
        if(sdk <= 25){
            register = new ActivityNotRegister();
        }else if(sdk <= 27){
            register = new ActivityNotRegister2();
        }else if(sdk <= 28){
            register = new ActivityNotRegister3();
        }else if(sdk <= 29){
            register = new ActivityNotRegister4();
        }else {
            register = new ActivityNotRegister5();
        }
        boolean result = register.init(context);
        LogHelper.i(result);
    }

    /**
     * 更改加载数据
     * */
    private static void changeDexOrder(Context context, String path){

        ClassLoader basicClassLoader = context.getClassLoader() ;
        ClassLoader extraClassLoader = new DexClassLoader(path, context.getCacheDir().getAbsolutePath(),null, basicClassLoader);
        LoadDexUtils.changeDexOrder(basicClassLoader, extraClassLoader);
    }


    /**
     * 使用 PackageParser 解析APK 可以解析APk
     *
     * 可以获取 APK名称和图片（大部分是id）, 然后在加载AssetManage获取对应的资源
     * */
    private static String getFirstClassName(String path){

        try {
            Class<?> packageParserClass = Class.forName("android.content.pm.PackageParser") ;
            Object packageParser = packageParserClass.newInstance();

            Method parsePackageMethod = packageParserClass.getMethod("parsePackage", File.class, int.class) ;

            Object package2 = parsePackageMethod.invoke(packageParser, new File(path) ,0);
            LogHelper.i(package2);

            Class<?> packageClass = Class.forName("android.content.pm.PackageParser$Package");
            Field activitiesField = packageClass.getField("activities") ;
            ArrayList activities = (ArrayList) activitiesField.get(package2);
            if(activities != null){
                final int size = activities.size();
                Class<?> componentClass = Class.forName("android.content.pm.PackageParser$Component");
                Field intentsField = componentClass.getField("intents") ;

                for (int i = 0; i < size; i++) {

                    Object activity = activities.get(i);
                    ArrayList<IntentFilter> intents= (ArrayList<IntentFilter>) intentsField.get(activity);

                    if(intents != null){
                        for (IntentFilter intent : intents) {

                            if(intent.hasAction("android.intent.action.MAIN")){
                                Field classNameField = componentClass.getField("className") ;

                                return (String) classNameField.get(activity);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
