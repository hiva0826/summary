package com.hiva.clazz.utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;

import dalvik.system.BaseDexClassLoader;

public class LoadDexUtils {


    /**
     * 更改加载顺序
     * @param basicClassLoader 原先的加载类型
     * @param extraClassLoader 额外的加载类
     * */
    public static void changeDexOrder(ClassLoader basicClassLoader, ClassLoader extraClassLoader){

        Object basicPathList = findPathList(basicClassLoader) ;
        Field basicDexElementsField = findDxElementsField() ;
        Object[] basicDxElements = getDxElements(basicDexElementsField, basicPathList);

        Object[] extraDxElements = getDxElements(basicDexElementsField, findPathList(extraClassLoader));

        Object[] combinationElements = combinationElements(basicDxElements, extraDxElements);

        if(basicDxElements != combinationElements){
            setDxElements(basicDexElementsField, basicPathList, combinationElements);
        }

    }

    private static Object findPathList(ClassLoader classLoader) {

        if(classLoader != null){
            try {
                Field pathListField = BaseDexClassLoader.class.getDeclaredField("pathList") ;
                pathListField.setAccessible(true);

                return pathListField.get(classLoader);
            } catch (NoSuchFieldException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private static Field findDxElementsField() {
        try {
            Class<?> clazz = Class.forName("dalvik.system.DexPathList") ;
            Field dexElementsField = clazz.getDeclaredField("dexElements") ;
            dexElementsField.setAccessible(true);

            return dexElementsField;
        } catch (ClassNotFoundException | NoSuchFieldException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 查找 dexElements数据
     * */
    private static Object[] getDxElements(Field dexElementsField, Object pathList) {

        if(dexElementsField != null && pathList != null){
            try {
                dexElementsField.setAccessible(true);

                return (Object[]) dexElementsField.get(pathList);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private static Object[] combinationElements(Object[] basicDxElements,  Object[] extraDxElements) {

        if(extraDxElements != null){

            ArrayList<Object> newExtraDxElements = new ArrayList<>() ;
            for (Object extraDxElement : extraDxElements ) {
                if(!isExist(basicDxElements, extraDxElement)){
                    newExtraDxElements.add(extraDxElement);
                }
            }
            int newSize = newExtraDxElements.size();
            if(newSize > 0){
                Object[] combinationElements = Arrays.copyOf(basicDxElements, basicDxElements.length + newSize);
                System.arraycopy(basicDxElements, 0, combinationElements, newSize, basicDxElements.length);

                for (int i = 0; i < newSize; i++) {
                    combinationElements[i] = newExtraDxElements.get(i);
                }
                return combinationElements;
            }
        }

        return basicDxElements;
    }

    /**
     * 设置 dexElements列表数据
     * */
    private static boolean setDxElements(Field dexElementsField, Object pathList, Object[] dexElements) {

        if(dexElementsField != null && pathList != null){
            try {
                dexElementsField.set(pathList, dexElements);
                return true;
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    //
    private static boolean isExist (Object[] basicDxElements,  Object extraDxElement) {

        for (Object basicDxElement: basicDxElements ){
//            if(basicDxElement.equals(extraDxElement)){
            if(basicDxElement.toString().equals(extraDxElement.toString())){
                return true;
            }
        }
        return false;
    }

}
