package com.hiva.clazz;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.hiva.clazz.utils.ApkUtils;

import java.util.ArrayList;

/**
 * Create By HuangXiangXiang 2023/1/5
 */
public class FirstActivity extends Activity implements AdapterView.OnItemClickListener {


    private GridView mUninstallAppGvw;

    private ArrayList<UninstallApp> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_first);

        mUninstallAppGvw = findViewById(R.id.uninstall_app_gvw);
        mUninstallAppGvw.setOnItemClickListener(this);

        list = ApkUtils.loadUninstallApps(this);
        UninstallAppAdapter adapter = new UninstallAppAdapter(this, list);
        mUninstallAppGvw.setAdapter(adapter);

    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        UninstallApp uninstallApp = list.get(position);
        ApkUtils.startApK(this, uninstallApp);

//        Intent intent = new Intent();
//        intent.setClassName(this, uninstallApp.getFirstClassName()) ;
//        startActivity(intent);

    }
}
