package com.hiva.clazz;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class UninstallAppAdapter extends BaseAdapter {

    private final Context context;
    private final LayoutInflater inflater;
    private final ArrayList<UninstallApp> list;

    public UninstallAppAdapter( Context context,ArrayList<UninstallApp> list) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.list = list;
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if(convertView == null){
            convertView = inflater.inflate(R.layout.item_uninstall_app, parent, false);
            holder = new ViewHolder(convertView);

            convertView.setTag(holder);
        }else {

            holder = (ViewHolder) convertView.getTag();
        }
        holder.setData(list.get(position));

        return convertView;
    }


    private static class ViewHolder{

        private final ImageView iconIvw ;
        private final TextView labelTvw ;

        private ViewHolder(View view) {

            this.iconIvw = view.findViewById(R.id.icon_ivw) ;
            this.labelTvw = view.findViewById(R.id.label_tvw) ;
        }


        private void setData(UninstallApp uninstallApp) {

            this.iconIvw.setImageDrawable(uninstallApp.getIcon());
            this.labelTvw.setText(uninstallApp.getLabel());

        }
    }
}
