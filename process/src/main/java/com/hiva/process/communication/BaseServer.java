package com.hiva.process.communication;

import android.content.Context;
import android.os.Bundle;

public abstract class BaseServer {

    protected final Context context;
    public BaseServer(Context context){
        this.context = context;
    }

    public abstract void start(Bundle parameter);

    /**通知数据变化*/
    public void change(){}

}
