package com.hiva.process.communication.udp;

import android.content.Context;
import android.os.Bundle;

import com.hiva.helper.app.ToastUtils;
import com.hiva.helper.communicate.socket.UdpCommunicate;
import com.hiva.helper.log.LogHelper;
import com.hiva.process.communication.BaseServer;

import java.net.DatagramPacket;
import java.net.UnknownHostException;

public class UdpServer extends BaseServer {

    private UdpCommunicate udpCommunicate = null ;
    public UdpServer(Context context) {
        super(context);

        int localPort = 8899;
        String address = "127.0.0.1";
        int port = 0;
        try {
            udpCommunicate = new UdpCommunicate(localPort, address, port);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void start(Bundle parameter) {

        if(udpCommunicate != null){

            udpCommunicate.setReceiveDataListener(new UdpCommunicate.UdpReceiveDataListener() {
                @Override
                public void receiveData(DatagramPacket datagramPacket, byte[] bytes, int offset, int length) {

                    String text = new String(bytes, offset, length );
                    LogHelper.i(text);
                    ToastUtils.show(context, text);
                }

                @Override
                public void receiveData(byte[] bytes, int i, int i1) {

                }
            });
        }

    }
}
