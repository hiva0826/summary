package com.hiva.process.communication.tcp;

import android.content.Context;
import android.os.Bundle;

import com.hiva.helper.app.ToastUtils;
import com.hiva.helper.communicate.socket.TcpServerCommunicate;
import com.hiva.helper.log.LogHelper;
import com.hiva.process.communication.BaseServer;

public class TcpServer extends BaseServer {

    private final TcpServerCommunicate  serverCommunicate ;
    public TcpServer(Context context) {
        super(context);

        int localPort = 8899;
        serverCommunicate = new TcpServerCommunicate(localPort);
    }

    @Override
    public void start(Bundle parameter) {

        serverCommunicate.setReceiveDataListener(new TcpServerCommunicate.TcpServerReceiveDataListener() {
            @Override
            public void accept(TcpServerCommunicate.ClientSocket clientSocket) {

            }

            @Override
            public void disAccept(TcpServerCommunicate.ClientSocket clientSocket) {

            }

            @Override
            public void receiveData(TcpServerCommunicate.ClientSocket clientSocket, byte[] bytes, int offset, int length) {

                String text = new String(bytes, offset, length);
                LogHelper.i(text);
                ToastUtils.show(context, text);
            }

            @Override
            public void receiveData(byte[] bytes, int offset, int length) {

            }
        });

    }
}
