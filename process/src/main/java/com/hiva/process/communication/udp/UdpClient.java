package com.hiva.process.communication.udp;

import android.content.Context;

import com.hiva.helper.communicate.socket.UdpCommunicate;
import com.hiva.process.communication.BaseClient;

import java.net.UnknownHostException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class UdpClient extends BaseClient {

    private UdpCommunicate udpCommunicate = null ;
    public UdpClient(Context context) {
        super(context);

        int localPort = 0;// 本地端口为0，表示随机设置
        String address = "127.0.0.1";
        int port = 8899;
        try {
            udpCommunicate = new UdpCommunicate(localPort, address, port);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    private final Executor single = Executors.newSingleThreadExecutor();
    @Override
    public void sendMessage(String message) {

        single.execute(() -> {

            if(udpCommunicate != null){
                udpCommunicate.send(message.getBytes(), 0 , message.length());
            }
        });


    }
}
