package com.hiva.process.communication.file;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.os.FileObserver;
import android.os.Looper;
import android.widget.Toast;

import com.hiva.helper.app.ToastUtils;
import com.hiva.helper.log.LogHelper;
import com.hiva.process.communication.BaseServer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class FileServer extends BaseServer {

    public FileServer(Context context) {
        super(context);
    }

    private class MyFileObserver extends FileObserver {


        private final File file;
        public MyFileObserver(File file) {
            super(file.getPath(), CLOSE_WRITE);   // 仅仅监听写完数据后的关闭
            this.file = file;
        }

        @Override
        public void onEvent(int event0, String path) {

            LogHelper.i(event0 + " :  " + path);
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new FileReader(this.file));
                String message = reader.readLine();
                LogHelper.i(message);

                ToastUtils.show(context, message);

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if(reader != null){
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
    }

    private MyFileObserver observer;
    @Override
    public void start(Bundle parameter) {

        File root = context.getExternalFilesDir("temp");
        File f = new File(root, "message.txt") ;
        String path = f.getPath();
        LogHelper.i(path);

        if(observer != null){
            if(f.compareTo(observer.file) == 0){
                // 监听的文件相同 不做任何处理
                return;
            }else {
                observer.stopWatching();
            }
        }
        observer = new MyFileObserver(f); // 变量放在外面 否则容易被gc 回收不生效
        observer.startWatching();
    }
}
