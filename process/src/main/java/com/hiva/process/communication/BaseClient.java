package com.hiva.process.communication;

import android.content.Context;

public abstract class BaseClient {

    protected final Context context;
    public BaseClient(Context context){
        this.context = context;
    }


    public abstract void sendMessage(String message);


}
