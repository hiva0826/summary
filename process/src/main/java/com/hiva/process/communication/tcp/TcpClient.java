package com.hiva.process.communication.tcp;

import android.content.Context;

import com.hiva.helper.communicate.socket.TcpClientCommunicate;
import com.hiva.helper.communicate.socket.TcpServerCommunicate;
import com.hiva.helper.communicate.socket.UdpCommunicate;
import com.hiva.process.communication.BaseClient;

import java.net.UnknownHostException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class TcpClient extends BaseClient {

    private final TcpClientCommunicate clientCommunicate ;
    public TcpClient(Context context) {
        super(context);

        String address = "127.0.0.1";
        int port = 8899;
        clientCommunicate = new TcpClientCommunicate(address, port);
    }

    private final Executor single = Executors.newSingleThreadExecutor();
    @Override
    public void sendMessage(String message) {

        single.execute(() -> {

            if(clientCommunicate != null){
                clientCommunicate.send(message.getBytes(), 0 , message.length());
            }
        });


    }
}
