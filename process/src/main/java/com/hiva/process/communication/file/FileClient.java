package com.hiva.process.communication.file;

import android.content.Context;
import android.os.Environment;

import com.hiva.helper.log.LogHelper;
import com.hiva.process.communication.BaseClient;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileClient extends BaseClient {

    public FileClient(Context context) {
        super(context);
    }

    @Override
    public void sendMessage(String message) {

        File root = context.getExternalFilesDir("temp");
        File f = new File(root, "message.txt") ;
        LogHelper.i(f.getPath());

        FileWriter writer = null;
        try {
            writer = new FileWriter(f);
            writer.write(message);
            writer.flush();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(writer != null){
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


}
