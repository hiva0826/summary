package com.hiva.process;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.util.SparseArray;

import com.hiva.helper.log.LogHelper;
import com.hiva.process.communication.BaseServer;
import com.hiva.process.communication.file.FileServer;
import com.hiva.process.communication.tcp.TcpServer;
import com.hiva.process.communication.udp.UdpServer;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;


public class SecondServerService  extends Service {

    private static final String KEY_TYPE        = "type" ;

    public static final int VALUE_FILE = 1 ;
    public static final int VALUE_UDP = 2 ;
    public static final int VALUE_TCP = 3 ;


    public static void startService(Context context, int type, Bundle parameter){

        LogHelper.i();

        Intent intent = new Intent();
        if(parameter != null){
            intent.replaceExtras(parameter);
        }
        intent.putExtra(KEY_TYPE, type);

        intent.setPackage(context.getPackageName());
        intent.setAction("com.hiva.SECOND");
        context.startService(intent);

    }
    public static void startService(Context context, int type){

        LogHelper.i();

        Intent intent = new Intent();
        intent.putExtra(KEY_TYPE, type);
        intent.setPackage(context.getPackageName());
        intent.setAction("com.hiva.SECOND");
        context.startService(intent);
    }


    private final SparseArray<Class<? extends BaseServer>> types = new SparseArray<>();
    {
        types.put(VALUE_FILE, FileServer.class);
        types.put(VALUE_UDP, UdpServer.class);
        types.put(VALUE_TCP, TcpServer.class);

    }


    @Override
    public void onCreate() {
        super.onCreate();

        LogHelper
                .useAndroid(this)
                .setPrint(true)
                .setSave(true)
                .start();

        LogHelper.i();
    }



    @Override
    public IBinder onBind(Intent intent) {

        LogHelper.i();
        return null;
    }

    private final SparseArray<BaseServer> startServers = new SparseArray<>();
    @Override
     public int onStartCommand(Intent intent, int flags, int startId) {

        int type = intent.getIntExtra(KEY_TYPE, -1);
        BaseServer server = startServers.get(type);
        if(server == null){
            Class<? extends BaseServer> clazz = types.get(type);
            if(clazz != null){
                try {
                    Constructor<? extends BaseServer> constructor = clazz.getConstructor(Context.class) ;
                    Context context = this ;
                    server = constructor.newInstance(context);
                    server.start(intent.getExtras());

                    startServers.put(type, server);
                    LogHelper.i();

                } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }

        if(server != null){
            server.change();
        }


         return super.onStartCommand(intent, flags, startId);
     }

}
