package com.hiva.process;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import com.hiva.process.communication.file.FileClient;
import com.hiva.process.communication.tcp.TcpClient;
import com.hiva.process.communication.udp.UdpClient;


/**
 * Create By HuangXiangXiang 2022/11/25
 */
public class ProcessDemoActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_process_demo);

    }

    int number = 0;


    private FileClient file;
    public void file(View view) {

        SecondServerService.startService(this, SecondServerService.VALUE_FILE);

        if(file == null ){
            file = new FileClient(this);
            // 第一次 有可能没有响应，因为还没有准备好
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        file.sendMessage("Hello File " + (number++  ));

    }

    private UdpClient udp;
    public void udp(View view) {

        SecondServerService.startService(this, SecondServerService.VALUE_UDP);
        if(udp == null){
            udp = new UdpClient(this);
        }
        udp.sendMessage("Hello Udp " + (number++  ));
    }

    private TcpClient tcp;
    public void tcp(View view) {

        SecondServerService.startService(this, SecondServerService.VALUE_TCP);
        if(tcp == null){
            tcp = new TcpClient(this);
        }
        tcp.sendMessage("Hello Tcp " + (number++  ));
    }




}

