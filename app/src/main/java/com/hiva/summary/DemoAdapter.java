package com.hiva.summary;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Create By HuangXiangXiang 2022/11/25
 */
public class DemoAdapter extends BaseAdapter {

    private final Context context;
    private final LayoutInflater inflater;
    private final PackageManager manager;
    private final List<ResolveInfo> data;

    public DemoAdapter(Context context, List<ResolveInfo> data) {

        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.manager = context.getPackageManager();
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if(convertView == null){

            convertView = inflater.inflate(R.layout.item_demo, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);

        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        ResolveInfo resolveInfo = data.get(position);
        String label = (String) resolveInfo.loadLabel(manager); // 获得应用程序的Label

        holder.setData(label);

        return convertView;
    }


    private static class ViewHolder{

        private final TextView nameTvw ;

        public ViewHolder(View view){

            nameTvw = (TextView) view.findViewById(R.id.name_tvw);
        }


        public void setData(String label) {
            nameTvw.setText(label);
        }
    }
}
