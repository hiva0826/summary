package com.hiva.summary;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.GridView;

import com.hiva.helper.log.LogHelper;

import java.util.List;

public class MainActivity extends Activity {

    private GridView mAllGvw;
    private List<ResolveInfo> list ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        LogHelper
                .useAndroid(this)
                .setSave(true)
                .start();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        initData();



        new Thread(){

            @Override
            public void run() {

                while (true){

                    Window window = MainActivity.this.getWindow();
                    View view = window.getDecorView();
//                    LogHelper.i(window);

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
        }.start();

    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event){

        LogHelper.i(event);
        boolean result =  super.dispatchKeyEvent(event);
        LogHelper.i(result);
        return true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        LogHelper.i(event);
        return false;
    }

    private void initView() {
        mAllGvw = (GridView) findViewById(R.id.all_gvw);
    }

    private void initData() {
        list = queryResolveInfo();
        DemoAdapter adapter = new DemoAdapter(this, list);

        mAllGvw.setAdapter(adapter);
        mAllGvw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                ResolveInfo info = list.get(position);

                String packageName = info.activityInfo.packageName; // 获得应用程序的包名
                String activityName = info.activityInfo.name; // 获得该应用程序的启动Activity的name
                Intent intent = new Intent();
                intent.setClassName(packageName, activityName);
                startActivity(intent);
            }
        });
    }


    private List<ResolveInfo> queryResolveInfo() {

        Intent hardwareIntent = new Intent("com.hiva.Demo");
        return getPackageManager().queryIntentActivities(hardwareIntent, 0);
    }

}