package com.hiva.hotfix;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.hiva.hotfix.function.utils.HotFixResourceUtils;
import com.hiva.hotfix.function.utils.LoadDexUtils;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

import dalvik.system.BaseDexClassLoader;
import dalvik.system.DexFile;

public class HotFixProvider extends ContentProvider {


    public void startHotFix(String dexPath, com.hiva.hotfix.function.fix.DexFile dexFile) {

        ClassLoader classLoader = getClass().getClassLoader();
        try {
            Field pathListField = BaseDexClassLoader.class.getDeclaredField("pathList") ;
            pathListField.setAccessible(true);
            Object pathList = pathListField.get(classLoader);

            Class<?> clazz = Class.forName("dalvik.system.DexPathList") ;
            Field dexElementsField = clazz.getDeclaredField("dexElements") ;
            dexElementsField.setAccessible(true);
            Object[] dexElements = (Object[]) dexElementsField.get(pathList);

            int length = dexElements.length;
            Object[] copyDexElements = Arrays.copyOf(dexElements, length + 1);
            System.arraycopy(dexElements, 0, copyDexElements, 1, length);

            Object element = addElement(dexFile) ;
            Object element2 = addElement2(dexPath) ;
            copyDexElements[0] = element;

            dexElementsField.set(pathList, copyDexElements);

        } catch (ClassNotFoundException | NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

    }



    private Object addElement(com.hiva.hotfix.function.fix.DexFile dexFile){

        ClassLoader classLoader = dexFile.dexClassLoader;
        try {
            Field pathListField = BaseDexClassLoader.class.getDeclaredField("pathList") ;
            pathListField.setAccessible(true);
            Object pathList = pathListField.get(classLoader);

            Class<?> clazz = Class.forName("dalvik.system.DexPathList") ;
            Field dexElementsField = clazz.getDeclaredField("dexElements") ;
            dexElementsField.setAccessible(true);
            Object dexElement = dexElementsField.get(pathList);

            if(dexElement != null){
                Object[] dexElements = (Object[]) dexElement;
                if(dexElements.length > 0){
                    return dexElements[0];
                }
            }

        } catch (ClassNotFoundException | NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return null;
    }

    private Object addElement2(String dexPath){

        try {
            File dir = new File("");
            boolean isDirectory = false;
            File zip = null;
            DexFile dexFile = new DexFile(dexPath);
            Class<?> clazz = Class.forName("dalvik.system.DexPathList$Element");

            Constructor<?> cons = clazz.getConstructor(File.class, boolean.class, File.class, DexFile.class);
            return cons.newInstance(dir, isDirectory, zip, dexFile);

        } catch (ClassNotFoundException | IOException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

        return null;
    }


    @Override
    public boolean onCreate() {

//        Context context = getContext();
//        File file = new File(context.getExternalFilesDir("dex"), "app-debug.apk");
//        HotFixResourceUtils.hotFixResource(context, file.getPath() );

//        Context context = getContext();
//        File file = new File(context.getExternalFilesDir("dex"), "hotfix1.dex");
//        String dexPath = file.getPath();
//        com.hiva.hotfix.function.fix.DexFile dexFile = new com.hiva.hotfix.function.fix.DexFile(context, dexPath);
//
//        LoadDexUtils.changeDexOrder(getClass().getClassLoader(), dexFile.dexClassLoader) ;




//        Context context = getContext();
//
//        File file = new File(context.getExternalFilesDir("dex"), "hotfix1.dex");
//        String dexPath = file.getPath();
//        com.hiva.hotfix.function.fix.DexFile dexFile = new com.hiva.hotfix.function.fix.DexFile(context, dexPath);
//        startHotFix(dexPath, dexFile) ;

        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        return null;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }
}
