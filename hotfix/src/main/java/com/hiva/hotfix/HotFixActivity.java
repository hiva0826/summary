package com.hiva.hotfix;

import android.app.Activity;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.hiva.helper.file.CopyAssetFileUtils;
import com.hiva.helper.log.LogHelper;
import com.hiva.hotfix.function.BaseFunction;
import com.hiva.hotfix.function.IFix;
import com.hiva.hotfix.function.fix.BaseHotFix;
import com.hiva.hotfix.function.fix.HotFix1;
import com.hiva.hotfix.function.fix.HotFix2;
import com.hiva.hotfix.function.fix.HotFix3;
import com.hiva.hotfix.function.fix.HotFix4;
import com.hiva.hotfix.function.fix.HotFix5;
import com.hiva.hotfix.function.utils.HotFixResourceUtils;

import java.io.File;

/**
 * Create By HuangXiangXiang 2022/12/10
 */
public class HotFixActivity extends Activity {

    private TextView mResultTvw;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CopyAssetFileUtils.copy(this, "dex", getExternalFilesDir("dex"));

//        File file = new File("file:///android_asset/dex");
//        String[] list = file.list();

        baseFunction = new BaseFunction();

        setContentView(R.layout.activity_hotfix);
        mResultTvw = (TextView) findViewById(R.id.result_tvw);
    }

    private BaseFunction baseFunction;
    public void testFunction(View view) {

        if(baseFunction != null){
            String result = baseFunction.testFunction();
            mResultTvw.setText(result);
        }
    }


    /**
     *
     * */
    public void hotFix1(View view) {

        if(baseFunction != null){

            File file = new File(getExternalFilesDir("dex"), "hotfix1.dex") ;
            BaseHotFix hotFix = new HotFix1(this, file.getPath());

            BaseFunction baseFunction = hotFix.startHotFix(this.baseFunction);

            BaseFunction base = new BaseFunction();
            LogHelper.i(base);


            String result = baseFunction.testFunction();
            mResultTvw.setText(result);

            baseFunction.setName("王二");
            LogHelper.i(baseFunction.getName());

            baseFunction.setAge(36);
            LogHelper.i(baseFunction.getAge());

            String code = BaseFunction.getCode("code1");
            LogHelper.i(code);

            String code2 = BaseFunction.getCode2("code2");
            LogHelper.i(code2);

            Resources.Theme theme = getTheme();


        }
    }

    public void hotFix2(View view) {

        if(baseFunction != null){

            File file = new File(getExternalFilesDir("dex"), "hotfix2.dex") ;
            BaseHotFix hotFix = new HotFix2(this, file.getPath());

            BaseFunction baseFunction = hotFix.startHotFix(this.baseFunction);

            String result = baseFunction.testFunction();
            mResultTvw.setText(result);
        }
    }


    public void hotFix3(View view) {

        if(baseFunction != null){

            File file = new File(getExternalFilesDir("dex"), "hotfix3.dex") ;
            BaseHotFix hotFix = new HotFix3(this, file.getPath());

            IFix baseFunction = hotFix.startHotFix((IFix) this.baseFunction);

            String result = baseFunction.testFunction();
            mResultTvw.setText(result);
        }
    }

    private BaseFunction baseFunction4;
    private int index = 0;
    public void hotFix4(View view) {

        if(baseFunction != null && baseFunction4 == null){
            File file = new File(getExternalFilesDir("dex"), "hotfix4.dex") ;
            BaseHotFix hotFix = new HotFix4(this, file.getPath());
            baseFunction4 = hotFix.startHotFix(this.baseFunction);
        }

        if(baseFunction4 != null){

            baseFunction4.setName("张三");
            String result = baseFunction4.getName();
            mResultTvw.setText(result);

//            String result;
//            switch (index){
//                case 1:
//                    result = baseFunction4.testFunction1(null);
//                    break;
//                case 2:
//                    result = baseFunction4.testFunction2(null, true);
//                    break;
//                case 3:
//                    result = baseFunction4.testFunction3(null, true, 24);
//                    break;
//                default:
//                    result = baseFunction4.testFunction();
//                    break;
//            }
//            mResultTvw.setText(result);
//
//            index++;
//            index = index % 4;
        }
    }



    private BaseFunction baseFunction5;
    public void hotFix5(View view) {

        if (baseFunction != null && baseFunction5 == null) {
            File file = new File(getExternalFilesDir("dex"), "hotfix1.dex");
            BaseHotFix hotFix = new HotFix5(this, file.getPath());
            baseFunction5 = hotFix.startHotFix(this.baseFunction);
        }

        if (baseFunction5 != null) {

            String result = baseFunction5.testFunction();
            mResultTvw.setText(result);
        }
    }


    public void hotFix6(View view) {

//        File file = new File(getExternalFilesDir("dex"), "app-debug.apk");
//        HotFixResourceUtils.hotFixResource(this, file.getPath() );

//        HotFixResourceUtils.loadResourceValue(this, R.string.test_name);
//        HotFixResourceUtils.loadResourceValue(this, R.string.second_name);

        String result = getString(R.string.test_name);
        LogHelper.i(result);

        result = getString(R.string.second_name);
        LogHelper.i(result);
        mResultTvw.setText(result);
    }
}
