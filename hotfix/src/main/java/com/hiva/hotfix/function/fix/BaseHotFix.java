package com.hiva.hotfix.function.fix;

import android.content.Context;

import com.hiva.hotfix.function.BaseFunction;
import com.hiva.hotfix.function.IFix;

/**
 * Create By HuangXiangXiang 2022/12/12
 */
public abstract class BaseHotFix {

    protected final DexFile dexFile;
    public BaseHotFix(Context context, String dexPath){

        dexFile = new DexFile(context, dexPath);
    }

    public final Class<?> findClass(String className){
        return dexFile.findClass(className);
    }

    public BaseFunction startHotFix(BaseFunction baseFunction) {
        return null;
    }

    public IFix startHotFix(IFix baseFunction) {
        return null;
    }

}
