package com.hiva.hotfix.function;

import java.lang.reflect.Method;

/**
 * Create By HuangXiangXiang 2022/12/10
 */
public class BaseFunction implements IFix {

    public final static String CODE = "110" ;

    public static String getCode(String name){

        return "原始 Code : " + CODE + "," + name ;
    }

    public static String getCode2(String name){

        return "原始 Code : " + CODE + ","  + name ;
    }

    private String name = "张三";
    private boolean isMale = true;
    private int age = 18;


    private HotFixFunction hotFixFunction;
    public void setHotFixFunction(HotFixFunction hotFixFunction) {
        this.hotFixFunction = hotFixFunction;
    }

    public String testFunction(){

        if(hotFixFunction != null){
            Class<?> methodClass = new Object(){}.getClass() ;
            Method hotFixMethod = hotFixFunction.findHotFixMethod(methodClass);
            if(hotFixMethod != null){
                return (String) hotFixFunction.hotFix(hotFixMethod);
            }
        }
        return "原始方法" ;
    }

    public String testFunction1(String name){

        if(hotFixFunction != null){
            Class<?> methodClass = new Object(){}.getClass();
            Method hotFixMethod = hotFixFunction.findHotFixMethod(methodClass);
            if(hotFixMethod != null){
                return (String) hotFixFunction.hotFix(hotFixMethod, name);
            }
        }
        return "原始方法 : " + name ;
    }

    public String testFunction2(String name, boolean isMale){

        if(hotFixFunction != null){
            Class<?> methodClass = new Object(){}.getClass() ;
            Method hotFixMethod = hotFixFunction.findHotFixMethod(methodClass);
            if(hotFixMethod != null){
                return (String) hotFixFunction.hotFix(hotFixMethod, name, isMale);
            }
        }
        return "原始方法 : " + name  + "," + isMale;
    }

    public String testFunction3(String name, boolean isMale, int age){

        if(hotFixFunction != null){
            Class<?> methodClass = new Object(){}.getClass() ;
            Method hotFixMethod = hotFixFunction.findHotFixMethod(methodClass);
            if(hotFixMethod != null){
                return (String) hotFixFunction.hotFix(hotFixMethod, name, isMale, age);
            }
        }
        return "原始方法 : " + name  + "," + isMale+ "," + age;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {

        if(hotFixFunction != null){

            Class<?> methodClass = new Object(){}.getClass() ;
            Method hotFixMethod = hotFixFunction.findHotFixMethod(methodClass);
            if(hotFixMethod != null){
                hotFixFunction.hotFix(hotFixMethod, name);
                return;
            }
        }

        this.name = name;
    }

    public boolean isMale() {
        return isMale;
    }

    public void setMale(boolean male) {
        isMale = male;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "BaseFunction{" +
                "name='" + name + '\'' +
                ", isMale=" + isMale +
                ", age=" + age +
                '}';
    }
}
