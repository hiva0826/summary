package com.hiva.hotfix.function.fix;

import android.content.Context;

import com.hiva.hotfix.function.BaseFunction;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import dalvik.system.DexClassLoader;

/**
 * Create By HuangXiangXiang 2022/12/12
 */
public final class DexFile {

    public final DexClassLoader dexClassLoader;
    public DexFile(Context context, String dexPath){

        ClassLoader parent = context.getClassLoader();
        this.dexClassLoader = new DexClassLoader(dexPath, context.getCacheDir().getAbsolutePath(),null, parent);
    }

    /**
     * 从本地 文件中查找 类
     * */
    public final Class<?> findClass(String className){

        try {
            Method findClassMethod = ClassLoader.class.getDeclaredMethod("findClass", String.class);
            findClassMethod.setAccessible(true);

            return (Class<?>) findClassMethod.invoke(dexClassLoader, className);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }


}
