package com.hiva.hotfix.function.fix;

import android.content.Context;

import com.hiva.hotfix.function.BaseFunction;
import com.hiva.hotfix.function.IFix;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * Create By HuangXiangXiang 2022/12/12
 *
 * 只能修复接口
 *
 */
public class HotFix3 extends BaseHotFix{

    public HotFix3(Context context, String dexPath) {
        super(context, dexPath);
    }

    @Override
    public IFix startHotFix(IFix baseFunction) {

        String className = "com.hiva.hotfix.function.BaseFunction3";
        try {
            Class<?> newClazz = (Class<?>) findClass(className);

            Constructor<?> con = newClazz.getConstructor(Object.class);
            InvocationHandler handler = (InvocationHandler) con.newInstance(baseFunction);

            Class<?>[] interfaces = (Class<?>[]) baseFunction.getClass().getGenericInterfaces();
            return (IFix) Proxy.newProxyInstance(IFix.class.getClassLoader(), interfaces, handler);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return baseFunction;

    }



}
