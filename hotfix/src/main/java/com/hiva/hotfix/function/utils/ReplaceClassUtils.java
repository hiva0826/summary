package com.hiva.hotfix.function.utils;

import java.lang.reflect.Field;

/**
 * Create By HuangXiangXiang 2022/12/14
 */
public class ReplaceClassUtils {


    /**
     * 替换 对象中的class
     * 只能 对其中的方向有效
     *
     * 方法中的静态方法和静态属性无效（可以替换有效）
     *
     * 类中的属性也无效
     * */
    public static void replaceObjectClass(Object object, Class<?> oldClazz, Class<?> newClazz) {

        try {
            Field field = Object.class.getDeclaredField("shadow$_klass_") ;
            field.setAccessible(true);
            field.set(object, newClazz);

            replaceClass(oldClazz, newClazz);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 修改修改类的静态 方法和静态属性
     * 不同版本 属性名称不一样
     *
     * */
    private static void replaceClass(Class<?> oldClazz, Class<?> newClazz) throws NoSuchFieldException, IllegalAccessException {

//        replaceValue(oldClazz, newClazz, "classLoader");
//        replaceValue(oldClazz, newClazz, "componentType");
//        replaceValue(oldClazz, newClazz, "dexCache");
//        replaceValue(oldClazz, newClazz, "dexCacheStrings");
//        replaceValue(oldClazz, newClazz, "ifTable");
//        replaceValue(oldClazz, newClazz, "name");
//        replaceValue(oldClazz, newClazz, "superClass");
//        replaceValue(oldClazz, newClazz, "verifyErrorClass");
//        replaceValue(oldClazz, newClazz, "vtable");
//        replaceValue(oldClazz, newClazz, "accessFlags");
        replaceValue(oldClazz, newClazz, "directMethods");  // 静态方法的地址
//        replaceValue(oldClazz, newClazz, "iFields");        // 属性的地址
        replaceValue(oldClazz, newClazz, "sFields");        // 静态属性的地址
//        replaceValue(oldClazz, newClazz, "virtualMethods"); // 方法的地址
//        replaceValue(oldClazz, newClazz, "classSize");
//        replaceValue(oldClazz, newClazz, "clinitThreadId");
//        replaceValue(oldClazz, newClazz, "dexClassDefIndex");
//        replaceValue(oldClazz, newClazz, "dexTypeIndex");
//        replaceValue(oldClazz, newClazz, "numDirectMethods");
//        replaceValue(oldClazz, newClazz, "numInstanceFields");
//        replaceValue(oldClazz, newClazz, "numReferenceInstanceFields");
//        replaceValue(oldClazz, newClazz, "numReferenceStaticFields");
//        replaceValue(oldClazz, newClazz, "numStaticFields");
//        replaceValue(oldClazz, newClazz, "numVirtualMethods");
//        replaceValue(oldClazz, newClazz, "objectSize");
//        replaceValue(oldClazz, newClazz, "primitiveType");
//        replaceValue(oldClazz, newClazz, "referenceInstanceOffsets");
//        replaceValue(oldClazz, newClazz, "status");

    }

    private static void replaceValue(Class<?> oldClazz, Class<?> newClazz, String fieldName ) throws NoSuchFieldException, IllegalAccessException {

            Field field = Class.class.getDeclaredField(fieldName) ;
            field.setAccessible(true);

            Object value = field.get(newClazz);
            field.set(oldClazz, value);
    }


}
