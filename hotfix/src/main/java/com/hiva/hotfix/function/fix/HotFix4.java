package com.hiva.hotfix.function.fix;

import android.content.Context;

import com.hiva.hotfix.function.BaseFunction;
import com.hiva.hotfix.function.HotFixFunction;

import java.lang.reflect.Constructor;

/**
 * Create By HuangXiangXiang 2022/12/12
 *
 *
 */
public class HotFix4 extends BaseHotFix{

    public HotFix4(Context context, String dexPath) {
        super(context, dexPath);
    }

    @Override
    public BaseFunction startHotFix(BaseFunction baseFunction) {

        String className = "com.hiva.hotfix.function.BaseFunction4";
        Class<?> clazz = findClass(className);
        try {
            Constructor<?> con = clazz.getConstructor(BaseFunction.class);
            Object hotFixObject = con.newInstance(baseFunction) ;

            HotFixFunction hotFixFunction = new HotFixFunction(hotFixObject);
            baseFunction.setHotFixFunction(hotFixFunction);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return baseFunction;
    }



}
