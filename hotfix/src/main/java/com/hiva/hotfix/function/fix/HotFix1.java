package com.hiva.hotfix.function.fix;

import android.content.Context;

import com.hiva.hotfix.function.BaseFunction;
import com.hiva.hotfix.function.utils.ReplaceClassUtils;

import java.lang.reflect.Field;

/**
 * Create By HuangXiangXiang 2022/12/12
 * 替换 Object 对象的 shadow$_klass_ 可以直接替换类中的一些方法
 * 缺点：不能替换属性 和 静态方法
 *
 */
public class HotFix1 extends BaseHotFix {

    public HotFix1(Context context, String dexPath) {
        super(context, dexPath);
    }

    @Override
    public BaseFunction startHotFix(BaseFunction baseFunction) {

        Class<?> oldClazz = baseFunction.getClass();

        String className = oldClazz.getName();
        Class<?> newClazz = findClass(className);

        if(newClazz != null && newClazz != oldClazz){
            ReplaceClassUtils.replaceObjectClass(baseFunction, oldClazz, newClazz);
        }

        return baseFunction;
    }
}
