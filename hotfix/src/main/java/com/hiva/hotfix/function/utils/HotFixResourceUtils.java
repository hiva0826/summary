package com.hiva.hotfix.function.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;

import com.hiva.helper.log.LogHelper;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Create By HuangXiangXiang 2022/12/15
 *
 * 热修复资源
 * 所有使用是的资源都是来自AssetManager,
 * 可以通过new一个新的，然后通过先添加额外的，最后添加系统的
 * 最后将AssetManager设置给Context
 *
 *
 * 本来想通过走旧的，加旧点添加路径，在修改顺序，实际上真正数据在native层，这样操作无效
 *
 */
public class HotFixResourceUtils {


    public static void hotFixResource(Context context, String path){

        try {

            final Class<AssetManager> assetManagerClass = AssetManager.class;

            AssetManager assetManager = assetManagerClass.newInstance();// 由于方法隐藏了只能通过反射

            Method addAssetPathMethod = assetManagerClass.getMethod("addAssetPath", String.class);
            addAssetPathMethod.invoke(assetManager, path); // 添加额外

            addAssetPathMethod.invoke(assetManager, context.getPackageResourcePath()); // 添加系统的

            // 设置资源
            Resources resources = context.getResources();
            Field mAssetsField = Resources.class.getDeclaredField("mAssets") ;
            mAssetsField.setAccessible(true);
            mAssetsField.set(resources, assetManager);


            // 设置 样式
            Resources.Theme theme = context.getTheme();
            mAssetsField = Resources.Theme.class.getDeclaredField("mAssets") ;
            mAssetsField.setAccessible(true);
            mAssetsField.set(theme, assetManager);

            LogHelper.i();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }




}
