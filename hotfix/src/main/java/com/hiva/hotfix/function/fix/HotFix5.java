package com.hiva.hotfix.function.fix;

import android.content.Context;

import com.hiva.hotfix.function.BaseFunction;

/**
 * Create By HuangXiangXiang 2022/12/12
 * 直接替换加载顺序
 *
 */
public class HotFix5 extends BaseHotFix{

    private String dexPath;
    public HotFix5(Context context, String dexPath) {
        super(context, dexPath);
        this.dexPath = dexPath;
    }

    @Override
    public BaseFunction startHotFix(BaseFunction baseFunction) {

        return baseFunction;
    }


}
