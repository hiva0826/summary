package com.hiva.hotfix.function.fix;

import android.content.Context;

import com.hiva.hotfix.function.BaseFunction;

import java.lang.reflect.Method;

import dalvik.system.DexClassLoader;

/**
 * Create By HuangXiangXiang 2022/12/12
 *
 * 继承后覆盖
 *
 */
public class HotFix2 extends BaseHotFix {

    public HotFix2(Context context, String dexPath) {
        super(context, dexPath);
    }

    @Override
    public BaseFunction startHotFix(BaseFunction baseFunction) {

        String className = "com.hiva.hotfix.function.BaseFunction2";
        try {
            Class<?> newClazz = findClass(className);

            BaseFunction newObj = (BaseFunction) newClazz.newInstance();

            newObj.setName(baseFunction.getName());
            newObj.setMale(baseFunction.isMale());
            newObj.setAge(baseFunction.getAge());

            return (BaseFunction) newObj;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return baseFunction;
    }



}
