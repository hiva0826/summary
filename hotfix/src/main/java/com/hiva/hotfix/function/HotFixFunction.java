package com.hiva.hotfix.function;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Create By HuangXiangXiang 2022/12/13
 */
public final class HotFixFunction {

    /**
     * 热修复的类
     * */
    private final Object hotFixObject;
    public HotFixFunction(Object hotFixObject) {
        this.hotFixObject = hotFixObject;
    }

    /**
     * 查找热修复的方法
     * @param methodClass 修复的类型
     *
     * */
    public final Method findHotFixMethod(Class<?> methodClass) {

        try {
            Method method = methodClass.getEnclosingMethod();
            String methodName = method.getName();
            Class<?>[] parameterTypes = method.getParameterTypes();

            Method hotFixMethod = hotFixObject.getClass().getDeclaredMethod(methodName, parameterTypes);
            hotFixMethod.setAccessible(true);

            return hotFixMethod;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * 开始热修复
     * */
    public final Object hotFix(Method hotFixMethod, Object... parameter) {

        try {
            return hotFixMethod.invoke(hotFixObject, parameter);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

}
