package com.hiva.service;

import android.app.Activity;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.view.View;

import com.hiva.helper.log.LogHelper;

/**
 * Create By HuangXiangXiang 2022/11/28
 */
public class SecondActivity extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogHelper.i();
        setContentView(R.layout.service_first);

        final IBinder binder = ServiceManager.getService(getPackageName());
        LogHelper.i(binder);
        if(binder != null){

            new Thread(){
                @Override
                public void run() {

                    int index = 0;
                    while (!isFinishing()){

                        Parcel data = Parcel.obtain();
                        Parcel reply = Parcel.obtain();
                        int flags = SecondService.FLAG_SEND;

                        try {
                            String text = "Hello Service " + (index++);
                            data.writeString(text);
                            binder.transact(1, data, reply, flags) ; // 传送数据
                            String result = reply.readString();
                            LogHelper.i(result);

                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }finally {
                            data.recycle();
                            reply.recycle();
                        }

                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                }
            }.start();

        }



    }

    public void send(View view) {


    }
}


