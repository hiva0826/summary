package com.hiva.service;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.MemoryFile;
import android.os.Parcel;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.view.View;
import android.widget.Toast;

import com.hiva.helper.log.LogHelper;

import java.io.IOException;

/**
 * Create By HuangXiangXiang 2022/11/28
 */
public class FirstActivity extends Activity {


    private IBinder binder;
    private final ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            binder = service;
            LogHelper.i(binder);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            LogHelper.i();
        }
    };



    private void bindService(Context context){

        Intent service = new Intent();
        service.setPackage(context.getPackageName());
        service.setAction("com.hiva.service.SECOND") ;

        context.bindService(service, connection, Context.BIND_AUTO_CREATE);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogHelper.i();
        setContentView(R.layout.service_first);
    }

    public void send(View view) {

        if(binder == null){
            bindService(this);

        }else {
            int code = SecondService.KEY_SEND;
            Parcel data = Parcel.obtain();
            Parcel reply = Parcel.obtain();
            int flags = SecondService.FLAG_SEND;

            try {
                String text = "Hello Service" ;
                data.writeString(text)
                ;
                binder.transact(code, data, reply, flags) ; // 传送数据
                SecondService.Result result = reply.readParcelable(SecondService.Result.class.getClassLoader());
                LogHelper.i(result);

            } catch (RemoteException e) {
                e.printStackTrace();
            }finally {
                data.recycle();
                reply.recycle();
            }
        }
    }

    public void startSecondActivity(View view) {

        final Binder binder = new Binder(){

            int index = 0;
            @Override
            protected boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {

                LogHelper.i(code);
                String request = data.readString();
                LogHelper.i(request);
                if(reply != null){
                    reply.writeString("result " + (index++));
                }
                return true;
            }
        };
        try{
            ServiceManager.addService(getPackageName(), binder);
        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(this, "添加服务失败：" + e, Toast.LENGTH_SHORT).show();
            return;
        }

        Intent intent = new Intent(this, SecondActivity.class);
        startActivity(intent);

    }
}


