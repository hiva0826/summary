package com.hiva.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.MemoryFile;
import android.os.Parcel;
import android.os.Parcelable;

import com.hiva.helper.log.LogHelper;

import java.io.FileDescriptor;
import java.io.IOException;

/**
 * Create By HuangXiangXiang 2022/11/28
 */
public class SecondService extends Service {

    public static final int KEY_SEND = 1;

    public static final int FLAG_SEND = 2;

    public static class Result implements Parcelable {

        public int code ;
        public String message;

        public Result(int code, String message) {
            this.code = code;
            this.message = message;
        }

        protected Result(Parcel in) {
            code = in.readInt();
            message = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(code);
            dest.writeString(message);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<Result> CREATOR = new Creator<Result>() {
            @Override
            public Result createFromParcel(Parcel in) {
                return new Result(in);
            }

            @Override
            public Result[] newArray(int size) {
                return new Result[size];
            }
        };

        @Override
        public String toString() {
            return code + "," + message ;
        }
    }


    @Override
    public void onCreate() {
        LogHelper.i();

        super.onCreate();
    }

    @Override
    public IBinder onBind(Intent intent) {
        LogHelper.i();
        return binder;
    }


    private final IBinder binder = new Binder(){

        @Override
        protected boolean onTransact(int code, Parcel data, Parcel reply, int flags) {
            LogHelper.i(code);

            String value = data.readString();
            LogHelper.i(value);

            Result result = new Result(0, "成功" );
            if(reply != null){
                reply.writeParcelable(result, flags);
            }

            return true;
        }


    };
}
