package com.hiva.event_bus.bean;

/**
 * Create By HuangXiangXiang 2022/12/23
 */
public class SecondEvent {

    public static SecondEvent getOne(String message){
        return new SecondEvent(message);
    }



    public final String message;
    public SecondEvent(String message) {
        this.message = message;
    }
}
