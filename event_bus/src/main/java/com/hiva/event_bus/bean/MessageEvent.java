package com.hiva.event_bus.bean;

/**
 * Create By HuangXiangXiang 2022/12/23
 */
public class MessageEvent {

    public static MessageEvent getOne(String message){
        return new MessageEvent(message);
    }

    public final String message;
    public MessageEvent(String message) {
        this.message = message;
    }
}
