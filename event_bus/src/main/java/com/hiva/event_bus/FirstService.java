package com.hiva.event_bus;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.hiva.event_bus.bean.MessageEvent;
import com.hiva.event_bus.bean.SecondEvent;
import com.hiva.helper.log.LogHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Create By HuangXiangXiang 2022/12/23
 */
public class FirstService extends Service {



    @Override
    public IBinder onBind(Intent intent) {
        LogHelper.i();
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        LogHelper.i();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true)
    public void onMessageEvent(MessageEvent event) {
        LogHelper.i("接收" + event.message);
    }

    @Subscribe(sticky = true)
    public void onMessageEvent(SecondEvent event) {
        LogHelper.i("接收" + event.message);
    }

}
