package com.hiva.event_bus;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.hiva.event_bus.bean.MessageEvent;
import com.hiva.event_bus.bean.SecondEvent;
import com.hiva.helper.log.LogHelper;

import org.greenrobot.eventbus.EventBus;


public class FirstActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_event_bus);
    }

    int index = 0;
    public void send(View view){

        String message = "message : " + (index ++);
        LogHelper.i("发送信息" + message);

        EventBus.getDefault().postSticky(MessageEvent.getOne(message));
    }
    public void send2(View view){

        String message = "message : " + (index ++);
        LogHelper.i("2发送信息" + message);

        EventBus.getDefault().postSticky(SecondEvent.getOne(message));
    }

    public void jump(View view){

        Intent intent = new Intent(this, FirstService.class);
        startService(intent);
    }

    public void jump2(View view){

        Intent intent = new Intent(this, SecondService.class);
        startService(intent);
    }

}
