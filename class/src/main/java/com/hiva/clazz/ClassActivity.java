package com.hiva.clazz;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import com.hiva.helper.log.LogHelper;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Create By HuangXiangXiang 2023/1/5
 */
public class ClassActivity extends Activity  {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class);
    }
    private final String path = "/dev/input/event0" ;
    private final Executor single = Executors.newFixedThreadPool(10);
    public void testAlarm(View view){

        single.execute(this::read);
    }
    public void read(){

        String cmd = "cat " + path;
        LogHelper.i(cmd);

        BufferedReader reader = null ;
        BufferedReader errorReader = null ;
        Runtime runtime = Runtime.getRuntime() ;
        try {
            Process process = runtime.exec(cmd) ;

            reader = new BufferedReader(new InputStreamReader(process.getInputStream())) ;
            String line ;
            while ((line = reader.readLine()) != null){
                LogHelper.i(line);
            }

            // 正常情况2选一
            errorReader = new BufferedReader(new InputStreamReader(process.getErrorStream())) ;
            while ((line = errorReader.readLine()) != null){
                LogHelper.i(line);
            }

            process.waitFor() ;
        } catch (Exception e) {
        } finally {
            if(reader != null){
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if(errorReader != null){
                try {
                    errorReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }



//        try {
//
//            byte[] data = new byte[100];
//            BufferedInputStream reader = new BufferedInputStream(new FileInputStream(path));
//            int line = reader.read(data);
//            LogHelper.i(line);
//            reader.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

    }

}
