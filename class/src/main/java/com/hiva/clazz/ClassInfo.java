package com.hiva.clazz;

/**
 * Create By HuangXiangXiang 2023/1/6
 */
public class ClassInfo {

    private byte[] magic = {(byte) 0xCA, (byte) 0xFE, (byte) 0xBA, (byte) 0xBE};
    private byte[] minor_version = {(byte) 0x00, (byte) 0x00};
    private byte[] major_version = {(byte) 0x00, (byte) 0x34};
    private byte[] constant_pool_count = {(byte) 0xCA, (byte) 0xFE};
    private CpInfo constant_pool = new CpInfo();
    private byte[] access_flags = {(byte) 0xCA, (byte) 0xFE};
    private byte[] this_class = {(byte) 0xCA, (byte) 0xFE};
    private byte[] super_class = {(byte) 0xCA, (byte) 0xFE};
    private byte[] interfaces_count = {(byte) 0xCA, (byte) 0xFE};
    private byte[] interfaces = {(byte) 0xCA, (byte) 0xFE};
    private byte[] fields_count = {(byte) 0xCA, (byte) 0xFE};
    private FieldInfo fields = new FieldInfo();
    private byte[] methods_count = {(byte) 0xCA, (byte) 0xFE};
    private MethodInfo methods = new MethodInfo();
    private byte[] attributes_count = {(byte) 0xCA, (byte) 0xFE};
    private AttributeInfo attributes = new AttributeInfo();


    private static class CpInfo{

    }

    private static class FieldInfo{

    }

    private static class MethodInfo{

    }

    private static class AttributeInfo{

    }

}
