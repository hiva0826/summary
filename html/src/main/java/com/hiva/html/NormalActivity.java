package com.hiva.html;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.TextView;

import com.hiva.helper.log.LogHelper;

public class NormalActivity extends Activity {

    private TextView resultTvw;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_normal);

        resultTvw = (TextView) findViewById(R.id.result_tvw);

        Intent intent = getIntent();
        Uri data = intent.getData();

        resultTvw.setText(data.getQueryParameter("name"));

        LogHelper.i(intent);


    }


    public static class One{

        int value ;

        public One plus(One other){

            value += other.value;
            return this;
        }
    }

}
