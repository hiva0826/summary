package com.hiva.html;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;


public class HtmlActivity extends Activity {

    private WebView mHtmlWvw;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_html);

        mHtmlWvw = (WebView) findViewById(R.id.html_wvw);
        mHtmlWvw.loadUrl("file:///android_asset/test.html");

    }
}
