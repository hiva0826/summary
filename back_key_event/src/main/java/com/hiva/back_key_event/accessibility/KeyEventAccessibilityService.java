package com.hiva.back_key_event.accessibility;

import android.accessibilityservice.AccessibilityService;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings.Secure;
import android.view.KeyEvent;
import android.view.accessibility.AccessibilityEvent;

import com.hiva.helper.log.LogHelper;


/**
 * Create By HuangXiangXiang 2023/1/11
 */
public class KeyEventAccessibilityService extends AccessibilityService {


    /**
     * 开启无障碍服务
     * Secure 修改数据需要 系统权限
     * */
    public static void startAccessibilityService(Context context, Class<? extends AccessibilityService> clazz){

        final ContentResolver resolver = context.getContentResolver();
        final int accessibilityEnabled = Secure.getInt(resolver, Secure.ACCESSIBILITY_ENABLED,0); // 获取是否开启
        if(accessibilityEnabled != 1){
            Secure.putInt(resolver, Secure.ACCESSIBILITY_ENABLED, 1);// 设置开启
        }

        final ComponentName selfComponentName = new ComponentName(context, clazz);
        final String flattenToString = selfComponentName.flattenToString(); // 当前当前服务

        String enabledAccessibilityServices = Secure.getString(resolver, Secure.ENABLED_ACCESSIBILITY_SERVICES); // 开启的服务
        if (enabledAccessibilityServices == null) {
            enabledAccessibilityServices = flattenToString;
            Secure.putString(resolver, Secure.ENABLED_ACCESSIBILITY_SERVICES, enabledAccessibilityServices);

        }else if(!isContain(enabledAccessibilityServices.split(":"), flattenToString)){
            enabledAccessibilityServices =  enabledAccessibilityServices + ":" + flattenToString;
            Secure.putString(resolver, Secure.ENABLED_ACCESSIBILITY_SERVICES, enabledAccessibilityServices);
        }
    }

    /**
     * 是否包含
     * */
    private static boolean isContain(String[] eas, String flattenToString){
        for (String es : eas) {
            if(es.equals(flattenToString)){
                return true;
            }
        }
        return false;
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
    }

    @Override
    public void onInterrupt() {
    }

    @Override
    protected boolean onKeyEvent(KeyEvent event) {

        LogHelper.i(event);

        return super.onKeyEvent(event);
    }



}
