package com.hiva.back_key_event.window;

import android.content.Context;
import android.graphics.PixelFormat;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;

import com.hiva.helper.log.LogHelper;

public class MonitorKeyEvent {

    // 全局上下文
    private final WindowManager windowManager;
    private final View monitorView;
    private final WindowManager.LayoutParams layoutParams;
    private final Callback callback;

    public MonitorKeyEvent(Context context, Callback callback) {

        this.windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        this.monitorView = new MonitorView(context);
        this.monitorView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                LogHelper.i(event);
                return true;
            }
        });

        this.layoutParams = new WindowManager.LayoutParams(
                1,1,
                WindowManager.LayoutParams.TYPE_TOAST,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
                ,
                PixelFormat.TRANSLUCENT
        );
        this.callback = callback;
    }


    public interface Callback {
        boolean dispatchKeyEvent(KeyEvent event);
    }

    private class MonitorView extends View{
        public MonitorView(Context context) {
            super(context);
        }

        @Override
        public boolean dispatchKeyEvent(KeyEvent event) {

            LogHelper.i(event);
            if(callback != null){
                boolean result = callback.dispatchKeyEvent(event);
                LogHelper.i(result);
                if(result){
                    return true;
                }
            }
            return super.dispatchKeyEvent(event);
        }
    }


    private boolean isStart = false;
    /**
     * 显示悬浮按钮
     */
    public void start() {

        LogHelper.i(isStart);
        if(!isStart){
            isStart = true;
            windowManager.addView(monitorView, layoutParams);
        }
    }


    /**
     * 隐藏悬浮按钮
     */
    public void stop() {

        LogHelper.i(isStart);
        if(isStart){
            isStart = false;
            windowManager.removeView(monitorView);
        }

    }

}

