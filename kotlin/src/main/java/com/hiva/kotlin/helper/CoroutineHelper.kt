package com.hiva.kotlin.helper

import androidx.lifecycle.LifecycleOwner
import com.hiva.helper.log.LogHelper
import kotlinx.coroutines.*
import kotlin.coroutines.EmptyCoroutineContext

/**
 *
 * Create By HuangXiangXiang 2022/12/23
 */
object CoroutineHelper {


    /**
     * 开始协程1
     * 通常适用于单元测试的场景，而业务开发中不会用到这种方法，因为它是线程阻塞的。
     * */
    fun startCoroutine1(code: Int){


        LogHelper.i(code)
        runBlocking {

            LogHelper.i(code)
            suspendFun(code)
            LogHelper.i(code)
        }
    }

    /**
     * 开始协程2
     *
     * 标准用法，我们可以通过 context 参数去管理和控制协程的生命周期
     * （这里的 context 和 Android 里的不是一个东西，是一个更通用的概念，会有一个 Android 平台的封装来配合使用），
     * CoroutineScope来创建协程的作用域
     * */
    fun startCoroutine2(code: Int){
        LogHelper.i(code)
        val coroutineScope = CoroutineScope(EmptyCoroutineContext)
        coroutineScope.launch {

            LogHelper.i(code)
            suspendFun(code)
            LogHelper.i(code)
        }

        val ctxHandler = CoroutineExceptionHandler { _, _ ->  }
        val context = Job() + Dispatchers.IO + EmptyCoroutineContext + ctxHandler
        CoroutineScope(context).launch {
        }


        MainScope().launch {


        }


        // 第一种写法
        coroutineScope.launch(Dispatchers.IO) {
            launch(Dispatchers.Main){
                launch(Dispatchers.IO) {
                    launch(Dispatchers.Main) {
                    }
                }
            }
        }


        // 通过第二种写法来实现相同的逻辑
        coroutineScope.launch(Dispatchers.Main) {
            withContext(Dispatchers.IO) {
            }
            withContext(Dispatchers.IO) {
            }
        }


    }


    /**
     * 开始协程3
     * GlobalScope是CoroutineScope的子类，使用场景先不考究。
     * */
    fun startCoroutine3(code: Int){

        LogHelper.i(code)
        GlobalScope.async {

            LogHelper.i(code)
            suspendFun(code)
            LogHelper.i(code)
            testQuery(object : OnResultListener {
                override fun onSuccess() {
                }
            })
        }

    }

    /**
     * 开始协程4
     * 方法四和方法三的区别，就在于launch和async的区别，这个稍后再分析
     * */
    fun startCoroutine4(code: Int){

        LogHelper.i(code)
        GlobalScope.async {

            LogHelper.i(code)
            suspendFun(code)
            LogHelper.i(code)
        }
    }

    fun startCoroutine5(code: Int){

        LogHelper.i()
        val context = Job() + Dispatchers.Default + CoroutineName(LogHelper.getTAG())
        CoroutineScope(context).launch {
            LogHelper.i()
        }
    }


    private suspend fun suspendFun(code: Int){

        LogHelper.i(code)
        delay(1000)
        LogHelper.i(code)
    }



    private suspend fun suspendFun2(code: Int){


    }

    interface OnResultListener {

        fun onSuccess()
    }


    private suspend fun testQuery(listener: OnResultListener){

        GlobalScope.async {



        }.await()

        listener.onSuccess()
    }

}