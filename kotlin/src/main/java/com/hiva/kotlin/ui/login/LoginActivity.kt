package com.hiva.kotlin.ui.login

import android.app.Activity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LifecycleCoroutineScope
import androidx.lifecycle.lifecycleScope
import com.hiva.helper.log.LogHelper
import com.hiva.kotlin.databinding.ActivityLoginBinding
import com.hiva.kotlin.helper.CoroutineHelper
import kotlinx.coroutines.*
import java.lang.Thread.sleep
import kotlin.coroutines.EmptyCoroutineContext

class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launch {

        }
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.start.setOnClickListener {
            start(index++)
        }

        binding.stop.setOnClickListener {

            stop()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        coroutineScope.cancel()
        LogHelper.i()
    }

    var coroutineScope: CoroutineScope = MainScope()

    private fun start(index: Int){
        LogHelper.i(index)
        coroutineScope.launch {
            test3()
        }
    }

    private suspend fun test3(){

        LogHelper.i(index)
        withContext(Dispatchers.IO){
            LogHelper.i(index)
            sleep(5000)
            LogHelper.i(index)
        }
        LogHelper.i(index)
        withContext(Dispatchers.IO){
            LogHelper.i(index)
            sleep(5000)
            LogHelper.i(index)
        }
        LogHelper.i(index)
        withContext(Dispatchers.Main){
            LogHelper.i(index)
            Toast.makeText(this@LoginActivity, "显示", Toast.LENGTH_SHORT).show()
            LogHelper.i(index)
        }
        LogHelper.i(index)

    }



    var index = 0
    private fun stop(){



    }






    private fun test0(){

        LogHelper.i()
        GlobalScope.launch {

            LogHelper.i()
            test1()
        }
    }

    private suspend fun test1(){

        withContext(Dispatchers.IO){
            LogHelper.i("第一次查询")
            sleep(3000)
            LogHelper.i("第一次查询结果")
        }

        withContext(Dispatchers.IO){
            LogHelper.i("第二次查询")
            sleep(3000)
            LogHelper.i("第二次查询结果")
        }

        withContext(Dispatchers.Main){
            LogHelper.i("运行结果")
        }
    }


    private fun test2(){
        LogHelper.i("第一次查询")
        val listener: OnResultListener = object : OnResultListener {
            override fun onSuccess() {
                LogHelper.i("第一次查询结果")

                LogHelper.i("第二次查询")
                val listener2: OnResultListener = object : OnResultListener {
                    override fun onSuccess() {
                        LogHelper.i("第二次查询结果")
                        runOnUiThread {
                            LogHelper.i("运行结果")
                        }
                    }
                }
                testQuery(listener2)
            }
        }
        testQuery(listener)
    }



    private interface OnResultListener {

        fun onSuccess()
    }

    private fun testQuery(listener: OnResultListener){
        Thread{
            sleep(3000)
            listener.onSuccess()
        }.start()
    }


   private suspend fun testQuery2(listener: OnResultListener) = withContext(Dispatchers.IO){

       sleep(3000)
       listener.onSuccess()
    }


}

