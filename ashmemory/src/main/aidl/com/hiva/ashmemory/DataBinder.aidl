package com.hiva.ashmemory;

import android.os.ParcelFileDescriptor;

import com.hiva.ashmemory.DataChangeListener;

import com.hiva.ashmemory.bean.MemoryFileParcelable;

interface DataBinder {


    /**
    * 监听数据变化
    */
    MemoryFileParcelable open(String key, int length, DataChangeListener listener);

    /**
    * 数据变化
    */
    void changeData(String key, int length);

    /**
    * 关闭 匿名内存
    */
    void close(String key);


    /**
    * 发送数据
    */
    void sendData(String key, out byte[] data);

}