package com.hiva.ashmemory;

interface DataChangeListener {

    /**
    * 数据变化
    */
    void onChangeData(String key);

}