package com.hiva.ashmemory;

import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;

import com.hiva.ashmemory.bean.AshmemInfo;
import com.hiva.ashmemory.bean.MemoryFileParcelable;
import com.hiva.helper.log.LogHelper;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

public class AshmemoryServerService extends Service {


    private final HashMap<String, AshmemInfo> map = new HashMap<>();

    private final DataBinder.Stub binder = new DataBinder.Stub() {

        @Override
        public MemoryFileParcelable open(String key, int length, DataChangeListener listener) {

            try {
                AshmemInfo ashmemInfo = map.get(key);
                if(ashmemInfo == null){
                    ashmemInfo = new AshmemInfo(key, length);
                    map.put(key, ashmemInfo);
                }else {

                    if(ashmemInfo.length != length) {
                        ashmemInfo.close();
                        map.remove(key);

                        ashmemInfo = new AshmemInfo(key, length);
                        map.put(key, ashmemInfo);
                    }
                }
                ashmemInfo.listener = listener;
                return ashmemInfo.mfp;

            } catch (IOException | NoSuchFieldException | IllegalAccessException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        public void changeData(String key, int length) {

            AshmemInfo ashmemInfo = map.get(key);
            if(ashmemInfo != null){
                ashmemInfo.changeData(length);
            }

        }

        @Override
        public void close(String key) {

            AshmemInfo ashmemInfo = map.remove(key);
            if(ashmemInfo != null){
                ashmemInfo.close();
            }

        }

        @Override
        public void sendData(String key, byte[] data) {

            LogHelper.i(key + " : " + new String(data));
        }


    };

    @Override
    public void onCreate() {
        super.onCreate();
        LogHelper.i();

        if (Build.VERSION.SDK_INT >= 29) {

            try {
                Class<?> clazz = Class.forName("org.chickenhook.restrictionbypass.Unseal") ;
                Method method = clazz.getMethod("unseal") ;
                method.invoke(null);

            } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }

        }
    }


    @Override
    public IBinder onBind(Intent intent) {
        LogHelper.i();
        return binder;
    }





}
