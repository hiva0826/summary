package com.hiva.ashmemory.bean;

import android.annotation.SuppressLint;
import android.os.MemoryFile;
import android.os.ParcelFileDescriptor;
import android.os.SharedMemory;
import android.system.ErrnoException;

import java.io.FileDescriptor;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

/**
 * Create By HuangXiangXiang 2022/12/12
 *
 * 27 以上 MemoryFile 文件
 *
 */
public class MemoryFileInfo2 extends MemoryFileInfo {


    public MemoryFileInfo2(MemoryFile memoryFile, ParcelFileDescriptor pfd) {
        super(memoryFile, pfd);
    }

    @Override
    protected ParcelFileDescriptor onParse(MemoryFile memoryFile){

        try {
            Field mSharedMemoryField = MemoryFile.class.getDeclaredField("mSharedMemory" );
            mSharedMemoryField.setAccessible(true);
            SharedMemory sharedMemory = (SharedMemory) mSharedMemoryField.get(memoryFile);

            Field mFileDescriptorField = SharedMemory.class.getDeclaredField("mFileDescriptor") ;
            mFileDescriptorField.setAccessible(true);
            FileDescriptor  fd = (FileDescriptor) mFileDescriptorField.get(sharedMemory);
            return ParcelFileDescriptor.dup(fd);
            
        } catch (NoSuchFieldException | IOException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected MemoryFile onTransform(MemoryFile memoryFile)   {

        try {
            Class<?> memoryFileClass = MemoryFile.class;
            FileDescriptor fd = pfd.getFileDescriptor();

            Constructor<SharedMemory> con = SharedMemory.class.getDeclaredConstructor(FileDescriptor.class);
            con.setAccessible(true);
            SharedMemory sharedMemory = con.newInstance(fd);

            Field mSharedMemoryField = memoryFileClass.getDeclaredField("mSharedMemory" );
            mSharedMemoryField.setAccessible(true);
            mSharedMemoryField.set(memoryFile, sharedMemory);

            Field mMappingField = memoryFileClass.getDeclaredField("mMapping" );
            mMappingField.setAccessible(true);
            mMappingField.set(memoryFile, sharedMemory.mapReadWrite());

            return memoryFile;
        } catch (InstantiationException | NoSuchMethodException | IllegalAccessException | InvocationTargetException | NoSuchFieldException | ErrnoException e) {
            e.printStackTrace();
        }

        return null;
    }

}
