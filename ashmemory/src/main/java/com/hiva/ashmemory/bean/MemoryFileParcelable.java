package com.hiva.ashmemory.bean;

import android.os.MemoryFile;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;

/**
 * Create By HuangXiangXiang 2022/11/29
 * MemoryFile 通讯使用
 */
public class MemoryFileParcelable implements Parcelable {

    private final ParcelFileDescriptor pfd;
    protected MemoryFileParcelable(Parcel in) {

        pfd = in.readParcelable(ParcelFileDescriptor.class.getClassLoader());
        initMemoryFileInfo(null, pfd);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(pfd, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MemoryFileParcelable> CREATOR = new Creator<MemoryFileParcelable>() {
        @Override
        public MemoryFileParcelable createFromParcel(Parcel in) {
            return new MemoryFileParcelable(in);
        }

        @Override
        public MemoryFileParcelable[] newArray(int size) {
            return new MemoryFileParcelable[size];
        }
    };

    /**包装一下memoryFileInfo信息*/
    private transient MemoryFileInfo memoryFileInfo;
    public MemoryFileParcelable(MemoryFile memoryFile)   {

        initMemoryFileInfo(memoryFile, null);
        this.pfd = this.memoryFileInfo.pfd;
    }

    private void initMemoryFileInfo(MemoryFile memoryFile, ParcelFileDescriptor pfd){

        if (android.os.Build.VERSION.SDK_INT >= 27) {
            this.memoryFileInfo = new MemoryFileInfo2(memoryFile, pfd) ;
        }else {
            this.memoryFileInfo = new MemoryFileInfo(memoryFile, pfd) ;
        }
    }

    public MemoryFile getMemoryFile() {

        return memoryFileInfo.getMemoryFile();
    }

}
