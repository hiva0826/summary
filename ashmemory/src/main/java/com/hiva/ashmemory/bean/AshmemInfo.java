package com.hiva.ashmemory.bean;

import android.os.MemoryFile;
import android.os.RemoteException;

import com.hiva.ashmemory.DataChangeListener;
import com.hiva.helper.log.LogHelper;

import java.io.IOException;

/**
 * Create By HuangXiangXiang 2022/11/29
 */
public class AshmemInfo {

    public final MemoryFileParcelable mfp ;
    public final String name;
    public final int length;

    public DataChangeListener listener;
    public AshmemInfo(String name, int length) throws IOException, NoSuchFieldException, IllegalAccessException {
        LogHelper.i();

        this.mfp = new MemoryFileParcelable(new MemoryFile(name, length));

        this.name = name;
        this.length = length;
    }

    public void close(){
        LogHelper.i();

        MemoryFile mf = mfp.getMemoryFile();
        if(mf != null){
            mf.close();
        }
    }

    public void changeData(int length) {

        LogHelper.i(length);
        if(length <= 0) return;
        MemoryFile memoryFile = mfp.getMemoryFile();
        try {
            byte[] data = new byte[length];
            memoryFile.readBytes(data, 0, 0, data.length);
            String text = new String(data);
            LogHelper.i(text);

            if(listener != null){
                listener.onChangeData(name);
            }

        } catch (IOException | RemoteException e) {
            e.printStackTrace();
        }
    }
}
