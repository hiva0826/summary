package com.hiva.ashmemory.bean;

import android.os.MemoryFile;
import android.os.ParcelFileDescriptor;

import com.hiva.helper.log.LogHelper;

import java.io.FileDescriptor;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Create By HuangXiangXiang 2022/12/12
 *
 * 25 26
 *
 */
public class MemoryFileInfo {

    private MemoryFile memoryFile;
    public final ParcelFileDescriptor pfd;

    public MemoryFileInfo(MemoryFile memoryFile,ParcelFileDescriptor pfd) {

        if(memoryFile == null){
            this.memoryFile = null;
            this.pfd = pfd;
        }else {
            this.memoryFile = memoryFile;
            this.pfd = onParse(memoryFile);
        }
    }


    public final MemoryFile getMemoryFile() {

        if(memoryFile == null){
            try {
                MemoryFile memoryFile = new MemoryFile("temp",1) ;
                memoryFile.close();

                this.memoryFile = onTransform(memoryFile);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return memoryFile;
    }


    protected ParcelFileDescriptor onParse(MemoryFile memoryFile) {

        try {
            // 反射获取 FileDescriptor
            Field mFDField = MemoryFile.class.getDeclaredField("mFD" );
            mFDField.setAccessible(true);
            FileDescriptor fd = (FileDescriptor) mFDField.get(memoryFile);
            return ParcelFileDescriptor.dup(fd);
        }catch (NoSuchFieldException | IllegalAccessException | IOException e){
            e.printStackTrace();
        }
        return null;
    }


    protected MemoryFile onTransform(MemoryFile memoryFile)  {

        try {
            Class<?> memoryFileClass = MemoryFile.class;

            FileDescriptor fd = pfd.getFileDescriptor();
            Method native_get_sizeMethod = memoryFileClass.getDeclaredMethod("native_get_size", FileDescriptor.class);

            native_get_sizeMethod.setAccessible(true);
            int length = (int) native_get_sizeMethod.invoke(null, fd);
            LogHelper.i(length);

            Method native_mmapMethod = memoryFileClass.getDeclaredMethod("native_mmap", FileDescriptor.class, int.class, int.class) ;
            native_mmapMethod.setAccessible(true);
            long address = (long) native_mmapMethod.invoke(null, fd, length, 3);
            LogHelper.i(address);

            Field mFDField = memoryFileClass.getDeclaredField("mFD" );
            mFDField.setAccessible(true);
            mFDField.set(memoryFile, fd);

            Field mAddressField = memoryFileClass.getDeclaredField("mAddress" );
            mAddressField.setAccessible(true);
            mAddressField.set(memoryFile, address);

            Field mLengthField = memoryFileClass.getDeclaredField("mLength" );
            mLengthField.setAccessible(true);
            mLengthField.set(memoryFile, length);
            return memoryFile;

        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | NoSuchFieldException e) {
            e.printStackTrace();
        }
        return null;
    }

}
