package com.hiva.ashmemory;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.MemoryFile;
import android.os.RemoteException;
import android.view.View;

import com.hiva.ashmemory.bean.MemoryFileParcelable;
import com.hiva.helper.log.LogHelper;

import java.io.IOException;

public class AshmemoryClientActivity extends Activity {


    private final ServiceConnection connection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {

            dataBinder = DataBinder.Stub.asInterface(service);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };


    private void bindService(Context context) {

        Intent service = new Intent();
        service.setPackage(context.getPackageName());
        service.setAction("com.hiva.ASHMEMORY");
        context.bindService(service, connection, Context.BIND_AUTO_CREATE);
    }

    private DataBinder dataBinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ashmemory_client);
        bindService(this);
    }

    private static final String KEY = "test";

    private MemoryFile memoryFile;

    public void changeData(View view) {

        if (dataBinder != null) {
            if (memoryFile == null) {
                DataChangeListener listener = new DataChangeListener.Stub() {
                    @Override
                    public void onChangeData(String key) {
                        LogHelper.i(key);
                    }
                };
                try {
                    MemoryFileParcelable mfp = dataBinder.open(KEY, 10 * 1024 * 1024, listener);
                    memoryFile = mfp.getMemoryFile();

                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }

            String text = (i++)+ " Hello AshMemory ";
            byte[] data = text.getBytes();
            int length = data.length;
            try {
                memoryFile.writeBytes(data, 0, 0, length);
                dataBinder.changeData(KEY, length);

            } catch (IOException | RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    int i = 0;
    public void sendData(View view) {

        if (dataBinder != null) {

            String text = (i++)+ " Hello AshMemory ";
            try {
                dataBinder.sendData(KEY, text.getBytes());
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

    }



}
